package main

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/server"
	"fmt"
	//	"net"
	l4g "code.google.com/p/log4go"
	"flag"
	"os"
	"time"
)

var configuration *config.Configuration

func main() {
	//parse commandline flags

	configFile := flag.String("configfile", "config.json", "json file with the configuration for the proxy")
	loggingFile := flag.String("logfile", "logging.xml", "xml file with the configuration for the logging")
	flag.Parse()

	fmt.Println("<Main>SMPPProxy Start")
	fmt.Println("<Main>Trying to load the logging.xml config file")
	l4g.LoadConfiguration(*loggingFile)
	fmt.Println("<Main>Switching to configured logger")
	l4g.Info("<Main>Starting SMPPProxy")

	l4g.Info("<Main>Loading config file")
	configuration = config.ParseConfigFile(*configFile)

	//	tcpAddr, err := net.ResolveTCPAddr("tcp4", configuration.Server)
	//	checkError(err)

	//	listener, err := net.ListenTCP("tcp", tcpAd)
	//	checkError(err)
	//	go waitForConnections(listener)

	server := server.ServerDetails{}
	server.Start(configuration)

	//	wait := make(chan int)
	//	var forever = <-wait
	//	l4g.Info("<Main>%d", forever)

	for {
		time.Sleep(5 * time.Second)
	}

}

func checkError(err error) {
	if err != nil {
		l4g.Error("<Main>Fatal error: %s", err.Error())
		time.Sleep(time.Second) //flush l4g buffers
		os.Exit(1)
	}
}
