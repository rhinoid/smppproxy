package protocol

type SmppEnquireLink struct {
}

func (smppEnquireLink *SmppEnquireLink) Validate() bool {
	//this PDU is so simple, there is nothing to validate
	return true
}

func (smppEnquireLink *SmppEnquireLink) Serialize() []byte {
	pdu := make([]byte, 0, 16)

	pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdEnquireLink)...) //command_id
	pdu = append(pdu, 0, 0, 0, 0)                                              //command_status
	pdu = append(pdu, 0, 0, 0, 0)                                              //seq number

	//prepend with length
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
