package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"strconv"
)

type Transceiver struct {
	ProtocolBase
	seqNrToLocalIDMap map[uint32]uint32
}

func (protocol *Transceiver) SocketConnected() {
	l4g.Info("<%s>Underlying socket appears to be connected. Sending bind command (and going to unbound state)", protocol.logTag)
	protocol.seqNrToLocalIDMap = make(map[uint32]uint32)
	protocol.changeState(ProtocolStateUnbound)
	protocol.sendBind(BindModeTranceiver)
}

func (protocol *Transceiver) HandleResponsePDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	seqNr := utils.ExtractIntegerFromArray(pdu[12:16])

	//check specifically for bind response
	switch commandId {
	case SmppCommandIdBindTranceiverResp:
		if protocol.state == ProtocolStateUnbound {
			protocol.changeState(ProtocolStateBound)
			l4g.Info("<%s>Connection Bound. Ready for transmit", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Bound PDU, while we are already bound!?! Ignoring this", protocol.logTag)
		}
		break
	case SmppCommandIdSubmitSMResp:
		fallthrough
	case SmppCommandIdDataSMResp:
		//get the real smsId from the actual response
		actualId := string(pdu[16:len(pdu)])

		//lookup localId in the map
		localId := protocol.seqNrToLocalIDMap[seqNr]

		//store the mapping from actualId to localId in Redis
		returnChannel := make(chan bool)
		protocol.databaseCounters.StoreAssociatedID(actualId, int(localId), returnChannel)

		l4g.Debug("<%s>Received submitsm_resp/datasm_resp with actualid: %s and found matching localId: %d.", protocol.logTag, actualId, localId)

		//Wait indefinately for the mapping to be stored
		_ = <-returnChannel

		l4g.Debug("<%s>actualid->localid association stored", protocol.logTag)

		break
	}

	//all other PDU responses we can just ignore

	//TODO: act on error codes
}

func (protocol *Transceiver) HandleIncomingPDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	sequenceNumber := utils.ExtractIntegerFromArray(pdu[12:16])

	switch commandId {
	case SmppCommandIdDeliverSM:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received submit_sm PDU", protocol.logTag)

			//get the real smsId from the actual command
			deliverSmPdu := SmppDeliverSM{}


			success, err := deliverSmPdu.parse(pdu, protocol.protocolConfig.ConvertDeliveryIdFromHexToDec)
			if !success {
				l4g.Error("<%s>Error parsing the DeliverSM PDU: %s", protocol.logTag, err.Error())
				protocol.sendDeliverSMReponse(sequenceNumber)    // should be send error to other side! (TODO)
				break
			}
			actualId := deliverSmPdu.receiptedMessageId

			//retrieve the mapping from actualId to localId in Redis
			returnChannel := make(chan int)
			protocol.databaseCounters.GetAssociatedID(actualId, returnChannel)

			l4g.Debug("<%s>Received deliver_sm with actualid: %s. Now going to reteive the associate localid in redis", protocol.logTag, actualId)

			//Wait indefinately for the mapping to be retrieved
			localId := <-returnChannel

			l4g.Debug("<%s>actualid->localid association: %s : %d", protocol.logTag, actualId, localId)

			//patch the PDU so we replace the actualId with the localId
			deliverSmPdu.setReceiptedMessageId("proxid" + strconv.Itoa(localId))
			patchedPdu := deliverSmPdu.Serialize()

			protocol.OutPDUStorageChannel <- patchedPdu
		} else {
			l4g.Warn("<%s>Received submit_sm PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendDeliverSMReponse(sequenceNumber)
		break
	case SmppCommandIdUnbind:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received Unbind PDU", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Unbind PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendResponse(sequenceNumber, commandId)
		protocol.changeState(ProtocolStateUnbound)
		break
	default:
		//Unrecognized or unpexpected PDU
		l4g.Error("<%s>Received PDU (%d(%s)) which we should not receive. Closing connection", protocol.logTag, commandId, commandIdToString(commandId))
		protocol.forceReconnect()
		break
	}

}

func (protocol *Transceiver) InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte {
	//check if the PDU is submit_sm
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	if commandId == SmppCommandIdSubmitSM || commandId == SmppCommandIdDataSM {
		//This is the PDU with the altered ID as the seqnr.
		//Strip out the localID
		localId := utils.ExtractIntegerFromArray(pdu[12:16])
		//Store this ID until we get the response from the real server
		protocol.seqNrToLocalIDMap[seqNr] = localId
		//And continue as normal (The localid will be wiped by the seqnr)
	}
	return pdu
}
