package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
)

type PassiveReceiver struct {
	ProtocolBase
}

func (protocol *PassiveReceiver) SocketConnected() {
	protocol.changeState(ProtocolStateUnbound)
}

func (protocol *PassiveReceiver) HandleResponsePDU(pdu []byte) {

	//all PDU responses we can just ignore

	//TODO: act on error codes
}

func (protocol *PassiveReceiver) HandleIncomingPDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	sequenceNumber := utils.ExtractIntegerFromArray(pdu[12:16])

	//wait for an incoming bind
	switch commandId {
	case SmppCommandIdBindReceiver:
		if protocol.state == ProtocolStateUnbound {
			protocol.changeState(ProtocolStateBound)
			l4g.Info("<%s>Connection Bound. Ready for receiving", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Bound PDU, while we are already bound? Ignoring this", protocol.logTag)
		}
		protocol.sendBindResponse(sequenceNumber, commandId)
		break
	case SmppCommandIdUnbind:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received Unbind PDU", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Unbind PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendResponse(sequenceNumber, commandId)
		protocol.changeState(ProtocolStateUnbound)
		break
	default:
		//Unrecognized or unpexpected PDU
		l4g.Error("<%s>Received PDU (%d(%s)) which we should not receive. Closing connection", protocol.logTag, commandId, commandIdToString(commandId))
		protocol.forceReconnect()
		break
	}
}

func (protocol *PassiveReceiver) InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte {
	return pdu
}
