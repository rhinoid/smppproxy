package protocol

type SmppDataSMResponse struct {
	smppStatus     uint32
	sequenceNumber uint32
	messageId      string
}

func (smppDataSMResponse *SmppDataSMResponse) Validate() bool {
	return len(smppDataSMResponse.messageId) <= 64
}

func (smppDataSMResponse *SmppDataSMResponse) Serialize() []byte {
	pdu := make([]byte, 0, 16)

	pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdSubmitSMResp)...)         //command_id
	pdu = append(pdu, 0, 0, 0, 0)                                                       //command_status
	pdu = append(pdu, createByteArrayFromInteger(smppDataSMResponse.sequenceNumber)...) //seq number
	pdu = append(pdu, []byte(smppDataSMResponse.messageId)...)                          //system_type
	pdu = append(pdu, 0)                                                                //endstring 0

	//prepend with length
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
