package protocol

type Tlv struct {
	Tag    uint16
	Length uint16
	Value  []byte
}
