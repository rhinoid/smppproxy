package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"strconv"
)

type Receiver struct {
	ProtocolBase
}

func (protocol *Receiver) SocketConnected() {
	l4g.Info("<%s>Underlying socket appears to be connected. Moving to unbound state", protocol.logTag)
	protocol.changeState(ProtocolStateUnbound)
	protocol.sendBind(BindModeReceiver)
}

func (protocol *Receiver) HandleResponsePDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])

	//check specifically for bind response
	if commandId == SmppCommandIdBindReceiverResp {
		if protocol.state == ProtocolStateUnbound {
			protocol.changeState(ProtocolStateBound)
			l4g.Info("<%s>Connection Bound. Ready for receiving", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Bound PDU, while we are already bound? Ignoring this", protocol.logTag)
		}
		return
	}

	//We don't specifically expect any PDU responses (apart from the bind) in receiver mode
	l4g.Error("<%s>Received response PDU for a PDU we didn't send in receiver mode!?! Let's reconnect", protocol.logTag)
	protocol.forceReconnect()

	//TODO: act on error codes
}

func (protocol *Receiver) HandleIncomingPDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	sequenceNumber := utils.ExtractIntegerFromArray(pdu[12:16])

	switch commandId {
	case SmppCommandIdDeliverSM:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received deliver_sm PDU", protocol.logTag)

			//get the real smsId from the actual command
			deliverSmPdu := SmppDeliverSM{}
			deliverSmPdu.parse(pdu, protocol.protocolConfig.ConvertDeliveryIdFromHexToDec)
			actualId := deliverSmPdu.receiptedMessageId

			//retrieve the mapping from actualId to localId in Redis
			returnChannel := make(chan int)
			protocol.databaseCounters.GetAssociatedID(actualId, returnChannel)

			l4g.Debug("<%s>Received deliver_sm with actualid: %s. Now going to reteive the associate localid in redis", protocol.logTag, actualId)

			//Wait indefinately for the mapping to be retrieved
			localId := <-returnChannel

			l4g.Debug("<%s>actualid->localid association: %s : %d", protocol.logTag, actualId, localId)

			//patch the PDU so we replace the actualId with the localId
			deliverSmPdu.setReceiptedMessageId("proxid" + strconv.Itoa(localId))
			patchedPdu := deliverSmPdu.Serialize()

			protocol.OutPDUStorageChannel <- patchedPdu
		} else {
			l4g.Warn("<%s>Received deliver_sm PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendDeliverSMReponse(sequenceNumber)
		break
	case SmppCommandIdUnbind:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received Unbind PDU", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Unbind PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendResponse(sequenceNumber, commandId)
		protocol.changeState(ProtocolStateUnbound)
		break
	default:
		//Unrecognized or unpexpected PDU
		l4g.Error("<%s>Received PDU (%d(%s)) which we should not receive. Closing connection", protocol.logTag, commandId, commandIdToString(commandId))
		protocol.forceReconnect()
		break
	}

}

func (protocol *Receiver) InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte {
	return pdu
}
