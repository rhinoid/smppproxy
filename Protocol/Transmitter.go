package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
)

type Transmitter struct {
	ProtocolBase
	seqNrToLocalIDMap map[uint32]uint32
}

func (protocol *Transmitter) SocketConnected() {
	l4g.Info("<%s>Underlying socket appears to be connected. Sending bind command (and going to unbound state)", protocol.logTag)
	protocol.seqNrToLocalIDMap = make(map[uint32]uint32)
	protocol.changeState(ProtocolStateUnbound)
	protocol.sendBind(BindModeTransmitter)
}

func (protocol *Transmitter) HandleResponsePDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	seqNr := utils.ExtractIntegerFromArray(pdu[12:16])

	//check specifically for bind response
	switch commandId {
	case SmppCommandIdBindTransmitterResp:
		if protocol.state == ProtocolStateUnbound {
			protocol.changeState(ProtocolStateBound)
			l4g.Info("<%s>Connection Bound. Ready for transmit", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Bound PDU, while we are already bound!?! Ignoring this", protocol.logTag)
		}
		break
	case SmppCommandIdSubmitSMResp:
	case SmppCommandIdDataSMResp:
		//get the real smsId from the actual response
		actualId := string(pdu[16:len(pdu)])

		//lookup localId in the map
		localId := protocol.seqNrToLocalIDMap[seqNr]

		//store the mapping from actualId to localId in Redis
		returnChannel := make(chan bool)
		protocol.databaseCounters.StoreAssociatedID(actualId, int(localId), returnChannel)

		l4g.Debug("<%s>Received submitsm_resp/datasm_resp with actualid: %s and found matching localId: %d.", protocol.logTag, actualId, localId)

		//Wait indefinately for the mapping to be stored
		_ = <-returnChannel

		l4g.Debug("<%s>actualid->localid association stored", protocol.logTag)

		break
	}

	//all other PDU responses we can just ignore

	//TODO: act on error codes
}

func (protocol *Transmitter) HandleIncomingPDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	//no incoming pdu's expected in transmitter mode
	l4g.Error("<%s>Received incoming PDU(%d(%s)) while in transmitter mode? Let's reconnect", protocol.logTag, commandId, commandIdToString(commandId))
	protocol.forceReconnect()
}

func (protocol *Transmitter) InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte {
	//check if the PDU is submit_sm
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	if commandId == SmppCommandIdSubmitSM || commandId == SmppCommandIdDataSM {
		//This is the PDU with the altered ID as the seqnr.
		//Strip out the localID
		localId := utils.ExtractIntegerFromArray(pdu[12:16])
		//Store this ID until we get the response from the real server
		protocol.seqNrToLocalIDMap[seqNr] = localId
		//And continue as normal (The localid will be wiped by the seqnr)
	}
	return pdu
}
