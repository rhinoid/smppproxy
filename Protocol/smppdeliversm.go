package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	"strings"
	"errors"
	"strconv"
	"fmt"
)

type SmppDeliverSM struct {
	smppStatus     uint32
	sequenceNumber uint32

	serviceType          string
	sourceAddrTon        byte
	sourceAddrNpi        byte
	sourceAddr           string
	destAddrTon          byte
	destAddrNpi          byte
	destinationAddr      string
	esmClass             byte
	protocolId           byte
	priorityFlag         byte
	scheduleDeliveryTime string
	validityPeriod       string
	registeredDelivery   byte
	replaceIfPresentFlag byte
	dataCoding           byte
	smDefaultMsgId       byte
	smLength             byte
	shortMessage         string

	receiptedMessageId string //the actual ID of the delivery notification. Extracted from the TLV or the payload
	messageState       byte   //the state of the delivery notification

	tlvs map[uint16]*Tlv
}

func (smppDeliverSM *SmppDeliverSM) parse(pdu []byte, convertDecToHexId bool) (success bool, err error) {
	if pdu == nil {
		return false, errors.New("There was no PDU supplied")
	}
	if len(pdu) < 16 {
		return false, errors.New("PDU was too small #1")
	}

	curIdx := 16


	smppDeliverSM.serviceType, err = utils.ExtractStringFromArray(pdu[curIdx:])
	if err != nil {
		return false, errors.New(err.Error() + "#1")
	}
	curIdx += len(smppDeliverSM.serviceType) + 1

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #2")
	}
	smppDeliverSM.sourceAddrTon = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #3")
	}
	smppDeliverSM.sourceAddrNpi = pdu[curIdx]
	curIdx++

	smppDeliverSM.sourceAddr, err = utils.ExtractStringFromArray(pdu[curIdx:])
	if err != nil {
		return false, errors.New(err.Error() + "#2")
	}
	curIdx += len(smppDeliverSM.sourceAddr) + 1

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #4")
	}
	smppDeliverSM.destAddrTon = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #5")
	}
	smppDeliverSM.destAddrNpi = pdu[curIdx]
	curIdx++

	smppDeliverSM.destinationAddr, err = utils.ExtractStringFromArray(pdu[curIdx:])
	if err != nil {
		return false, errors.New(err.Error() + "#3")
	}
	curIdx += len(smppDeliverSM.destinationAddr) + 1

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #6")
	}
	smppDeliverSM.esmClass = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #7")
	}
	smppDeliverSM.protocolId = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #8")
	}
	smppDeliverSM.priorityFlag = pdu[curIdx]
	curIdx++

	smppDeliverSM.scheduleDeliveryTime, err = utils.ExtractStringFromArray(pdu[curIdx:])
	if err != nil {
		return false, errors.New(err.Error() + "#4")
	}
	curIdx += len(smppDeliverSM.scheduleDeliveryTime) + 1

	smppDeliverSM.validityPeriod, err = utils.ExtractStringFromArray(pdu[curIdx:])
	if err != nil {
		return false, errors.New(err.Error() + "#5")
	}
	curIdx += len(smppDeliverSM.validityPeriod) + 1

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #9")
	}
	smppDeliverSM.registeredDelivery = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #10")
	}
	smppDeliverSM.replaceIfPresentFlag = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #11")
	}
	smppDeliverSM.dataCoding = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #12")
	}
	smppDeliverSM.smDefaultMsgId = pdu[curIdx]
	curIdx++

	if curIdx >= len(pdu) {
		return false, errors.New("PDU was too small #13")
	}
	smppDeliverSM.smLength = pdu[curIdx]
	curIdx++

	smppDeliverSM.shortMessage, err = utils.ExtractFixedLengthStringFromArray(pdu[curIdx:], int(smppDeliverSM.smLength))
	if err != nil {
		return false, errors.New(err.Error() + "#6")
	}
	curIdx += int(smppDeliverSM.smLength)

	smppDeliverSM.tlvs, err = extractTlvsFromPdu(pdu[curIdx:])
	if err != nil {
		return false,err
	}

	//try to extract the 'receiptedMessageId' from the TLV's
	smppDeliverSM.receiptedMessageId = ""
	smppDeliverSM.messageState = 255
	for _, tlvValue := range smppDeliverSM.tlvs {
		switch tlvValue.Tag {
		case TlvReceiptedMessageId:
			smppDeliverSM.receiptedMessageId = string(tlvValue.Value)
			break
		case TlvMessagePayload:
			smppDeliverSM.shortMessage = string(tlvValue.Value)
			break
		case TlvMessageState:
			smppDeliverSM.messageState = tlvValue.Value[0]
			break
		}
	}

	//add the short message as a tlv in case it doesn't contain it yet
	if smppDeliverSM.tlvs[TlvMessagePayload] == nil {
		tlv := new(Tlv)
		tlv.Tag = TlvMessagePayload
		tlv.Length = uint16(len(smppDeliverSM.shortMessage))
		tlv.Value = []byte(smppDeliverSM.shortMessage)
		smppDeliverSM.tlvs[tlv.Tag] = tlv
	}

	if len(smppDeliverSM.receiptedMessageId) == 0 || smppDeliverSM.messageState == 255 {
		//try and parse the payload message
		tokens := strings.Split(smppDeliverSM.shortMessage, " ")
		for _, token := range tokens {
			//check for 'id:'
			if strings.HasPrefix(token, "id:") && len(smppDeliverSM.receiptedMessageId) == 0 {
				parsedIdHex := token[3:]
				if convertDecToHexId {
					//expected id is not hex but decimal... let's parse it and convert it to hex
					idAsInt, err := strconv.Atoi(parsedIdHex)
					if err == nil {
						parsedIdHex = fmt.Sprintf("%x", idAsInt)
					} else {
						return false, errors.New("Can not convert ID returned in smppDeliverSM from decimal to hex. The id was: "+parsedIdHex)
					}
				}
				smppDeliverSM.receiptedMessageId = parsedIdHex

			} else if strings.HasPrefix(token, "stat:") && smppDeliverSM.messageState == 255 {
				messageState := token[5:]
				switch messageState {
				case "ENROUTE":
					smppDeliverSM.messageState = 1
				case "DELIVERED":
					smppDeliverSM.messageState = 2
				case "EXPIRED":
					smppDeliverSM.messageState = 3
				case "DELETED":
					smppDeliverSM.messageState = 4
				case "UNDELIVERABLE":
					smppDeliverSM.messageState = 5
				case "ACCEPTED":
					smppDeliverSM.messageState = 6
				case "UNKNOWN":
					smppDeliverSM.messageState = 7
				case "REJECTED":
					smppDeliverSM.messageState = 8
				default:
					smppDeliverSM.messageState = 7
				}

				tlv := new(Tlv)
				tlv.Tag = TlvMessageState
				tlv.Length = uint16(1)
				tlv.Value = []byte{smppDeliverSM.messageState}
				smppDeliverSM.tlvs[tlv.Tag] = tlv
			}

		}
	}

	return true, nil
}

func (smppDeliverSM *SmppDeliverSM) Validate() bool {
	return true
}

func (smppDeliverSM *SmppDeliverSM) Serialize() []byte {
	pdu := make([]byte, 0, 4096)

	pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdDeliverSM)...)
	pdu = append(pdu, 0, 0, 0, 0) //command_status
	pdu = append(pdu, 0, 0, 0, 0) //seq number (filled out when it's actually sent)

	//actual params
	pdu = append(pdu, []byte(smppDeliverSM.serviceType)...)
	pdu = append(pdu, 0) //endstring 0
	pdu = append(pdu, smppDeliverSM.sourceAddrTon)
	pdu = append(pdu, smppDeliverSM.sourceAddrNpi)
	pdu = append(pdu, []byte(smppDeliverSM.sourceAddr)...)
	pdu = append(pdu, 0) //endstring 0
	pdu = append(pdu, smppDeliverSM.destAddrTon)
	pdu = append(pdu, smppDeliverSM.destAddrNpi)
	pdu = append(pdu, []byte(smppDeliverSM.destinationAddr)...)
	pdu = append(pdu, 0) //endstring 0
	pdu = append(pdu, smppDeliverSM.esmClass)
	pdu = append(pdu, smppDeliverSM.protocolId)
	pdu = append(pdu, smppDeliverSM.priorityFlag)
	pdu = append(pdu, 0) //scheduledDeliveryTime must be set to NULL
	pdu = append(pdu, 0) //validityPeriod must be set to NULL
	pdu = append(pdu, smppDeliverSM.registeredDelivery)
	pdu = append(pdu, 0) //replaceIfPresent must be set to 0
	pdu = append(pdu, smppDeliverSM.dataCoding)
	pdu = append(pdu, 0) //smDefaultMsgId msut be set to 0
	pdu = append(pdu, 0) //regardless what the original smLength was, we set it to 0 and specify the MessageID using a tlv (mads supports this anyway)
	tlvPdus := serializeTlvs(smppDeliverSM.tlvs)
	pdu = append(pdu, tlvPdus...)
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}

func (smppDeliverSM *SmppDeliverSM) setReceiptedMessageId(messageId string) {
	smppDeliverSM.receiptedMessageId = messageId

	receiptedIdArray := []byte(smppDeliverSM.receiptedMessageId)
	nullTerminatedReceiptedIdArray := append(receiptedIdArray, 0)

	tlv := new(Tlv)
	tlv.Tag = TlvReceiptedMessageId
	tlv.Length = uint16(len(nullTerminatedReceiptedIdArray))
	tlv.Value = []byte(nullTerminatedReceiptedIdArray)
	smppDeliverSM.tlvs[tlv.Tag] = tlv
}
