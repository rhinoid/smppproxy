package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	"errors"
)

const (
	SmppCommandIdBindReceiver        = 1
	SmppCommandIdBindTransmitter     = 2
	SmppCommandIdQuerySM             = 3
	SmppCommandIdSubmitSM            = 4
	SmppCommandIdDeliverSM           = 5
	SmppCommandIdUnbind              = 6
	SmppCommandIdReplaceSM           = 7
	SmppCommandIdCancelSM            = 8
	SmppCommandIdBindTranceiver      = 9
	SmppCommandIdEnquireLink         = 21
	SmppCommandIdDataSM              = 0x103
	SmppCommandIdBindReceiverResp    = 1 | 0x80000000
	SmppCommandIdBindTransmitterResp = 2 | 0x80000000
	SmppCommandIdQuerySMResp         = 3 | 0x80000000
	SmppCommandIdSubmitSMResp        = 4 | 0x80000000
	SmppCommandIdDeliverSMResp       = 5 | 0x80000000
	SmppCommandIdUnbindResp          = 6 | 0x80000000
	SmppCommandIdReplaceSMResp       = 7 | 0x80000000
	SmppCommandIdCancelSMResp        = 8 | 0x80000000
	SmppCommandIdBindTranceiverResp  = 9 | 0x80000000
	SmppCommandIdEnquireLinkResp     = 21 | 0x80000000
	SmppCommandIdDataSMResp          = 0x103 | 0x80000000

	SmppCommandStatusThrottlingEsmeRThrottled = 0x00000058
)

const (
	TlvReceiptedMessageId = 0x001e // The TLV which can contain the id of the del notification
	TlvMessagePayload     = 0x0424 //The TLV which can contain the message payload
	TlvMessageState       = 0x0427 //The TLV which can contain the delivery notification state
)

type PDU interface {
	Validate() bool
	Serialize() utils.PDUBytes
}

func extractFirstPDU(data []byte) utils.PDUBytes {
	var extractedPDU []byte = nil
	//check if we have at the least the header of the PDU
	if len(data) < 16 {
		return nil
	}

	//read length octet from PDU header
	pduLength := utils.ExtractIntegerFromArray(data)
	if pduLength <= uint32(len(data)) {
		//PDU present! let's extract it
		extractedPDU = make([]byte, pduLength)
		copy(extractedPDU, data[0:pduLength])
	}
	return extractedPDU
}

func removeFirstPDU(data []byte) []byte {
	newDataBuffer := data

	//check if we have at the least the header of the PDU
	if len(data) < 16 {
		//nothing extracted
		return data
	}

	//read length octet from PDU header
	pduLength := utils.ExtractIntegerFromArray(data)
	if pduLength == uint32(len(data)) {
		newDataBuffer = nil
	} else if pduLength < uint32(len(data)) {
		//PDU present! let's remove it
		newDataBuffer = make([]byte, uint32(len(data))-pduLength)
		copy(newDataBuffer, data[pduLength:len(data)])
	}
	return newDataBuffer
}

func extractTlvsFromPdu(data []byte) (tlvs map[uint16]*Tlv, err error) {
	startIdx := 0

	tlvs = map[uint16]*Tlv{}

	for startIdx < len(data) {
		if startIdx+4 >= len(data) {
			//half a tlv?!?
			return tlvs, errors.New("TLV structure in PDU corrupt")
		}
		tag := utils.ExtractUint16FromArray(data[startIdx:])
		length := utils.ExtractUint16FromArray(data[startIdx+2:])
		if startIdx+4+int(length) > len(data) {
			return tlvs, errors.New("TLV data part is missing. PDU corrupt")
		}
		data := data[startIdx+4 : startIdx+4+int(length)]
		startIdx += 4 + int(length)
		tlv := new(Tlv)
		tlv.Tag = tag
		tlv.Length = length
		tlv.Value = data
		tlvs[tag] = tlv
	}
	return tlvs, err
}

func serializeTlvs(tlvs map[uint16]*Tlv) (data []byte) {
	pdu := make([]byte, 0, 4096)
	for _, tlv := range tlvs {
		pdu = append(pdu, createByteArrayFromUint16(tlv.Tag)...)

		pdu = append(pdu, createByteArrayFromUint16(tlv.Length)...)

		pdu = append(pdu, tlv.Value...)
	}

	return pdu
}

func createByteArrayFromInteger(number uint32) []byte {
	return []byte{byte((number & 0xff000000) >> 24), byte((number & 0xff0000) >> 16), byte((number & 0xff00) >> 8), byte(number & 0xff)}
}

func createByteArrayFromUint16(number uint16) []byte {
	return []byte{byte((number & 0xff00) >> 8), byte(number & 0xff)}
}

func commandIdToString(commandId uint32) string {
	switch commandId {
	case SmppCommandIdBindReceiver:
		return "SmppCommandIdBindReceiver"
	case SmppCommandIdBindTransmitter:
		return "SmppCommandIdBindTransmitter"
	case SmppCommandIdBindTranceiver:
		return "SmppCommandIdBindTranceiver"
	case SmppCommandIdCancelSM:
		return "SmppCommandIdCancelSM"
	case SmppCommandIdDeliverSM:
		return "SmppCommandIdDeliverSM"
	case SmppCommandIdEnquireLink:
		return "SmppCommandIdEnquireLink"
	case SmppCommandIdQuerySM:
		return "SmppCommandIdQuerySM"
	case SmppCommandIdReplaceSM:
		return "SmppCommandIdReplaceSM"
	case SmppCommandIdSubmitSM:
		return "SmppCommandIdSubmitSM"
	case SmppCommandIdUnbind:
		return "SmppCommandIdUnbind"
	case SmppCommandIdBindReceiverResp:
		return "SmppCommandIdBindReceiverResp"
	case SmppCommandIdBindTransmitterResp:
		return "SmppCommandIdBindTransmitterResp"
	case SmppCommandIdBindTranceiverResp:
		return "SmppCommandIdBindTranceiverResp"
	case SmppCommandIdCancelSMResp:
		return "SmppCommandIdCancelSMResp"
	case SmppCommandIdDeliverSMResp:
		return "SmppCommandIdDeliverSMResp"
	case SmppCommandIdEnquireLinkResp:
		return "SmppCommandIdEnquireLinkResp"
	case SmppCommandIdQuerySMResp:
		return "SmppCommandIdQuerySMResp"
	case SmppCommandIdReplaceSMResp:
		return "SmppCommandIdReplaceSMResp"
	case SmppCommandIdSubmitSMResp:
		return "SmppCommandIdSubmitSMResp"
	case SmppCommandIdUnbindResp:
		return "SmppCommandIdUnbindResp"

	}
	return "Unknown CommandID"
}
