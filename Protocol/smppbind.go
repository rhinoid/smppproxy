package protocol

import (
	l4g "code.google.com/p/log4go"
	"os"
	"time"
)

const (
	BindModeTransmitter = 1
	BindModeTranceiver  = 2
	BindModeReceiver    = 3

	MaxLenPassword     = 8
	MaxLenSystemId     = 15
	MaxLenSystemType   = 12
	MaxLenAddressRange = 40
)

type SmppBind struct {
	BindMode         int
	SystemId         string //max16
	Password         string //max9
	SystemType       string //max13
	InterfaceVersion uint8
	AddrTon          uint8
	AddrNpi          uint8
	AddressRange     string //max41
}

func (smppBind *SmppBind) Validate() bool {
	if len(smppBind.SystemId) > MaxLenSystemId {
		return false
	}
	if len(smppBind.Password) > MaxLenPassword {
		return false
	}
	if len(smppBind.SystemType) > MaxLenSystemType {
		return false
	}
	if len(smppBind.AddressRange) > MaxLenAddressRange {
		return false
	}
	return true
}

func (smppBind *SmppBind) Serialize() []byte {
	pdu := make([]byte, 0, 50)

	//command_id
	switch smppBind.BindMode {
	case BindModeTransmitter:
		pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdBindTransmitter)...)
	case BindModeTranceiver:
		pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdBindTranceiver)...)
	case BindModeReceiver:
		pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdBindReceiver)...)
	default:
		l4g.Critical("<Transmitter>Code error. Unknown bind type selected")
		time.Sleep(time.Second) //flush l4g buffers
		os.Exit(-1)
	}

	pdu = append(pdu, 0, 0, 0, 0) //command_status
	pdu = append(pdu, 0, 0, 0, 0) //seq number (filled out when it's actually sent)

	//actual bind params
	pdu = append(pdu, []byte(smppBind.SystemId)...)
	pdu = append(pdu, 0)                                //endstring 0
	pdu = append(pdu, []byte(smppBind.Password)...)     //password
	pdu = append(pdu, 0)                                //endstring 0
	pdu = append(pdu, []byte(smppBind.SystemType)...)   //system_type
	pdu = append(pdu, 0)                                //endstring 0
	pdu = append(pdu, smppBind.InterfaceVersion)        //interface_version
	pdu = append(pdu, 0)                                //addr_ton
	pdu = append(pdu, 0)                                //addr_npi
	pdu = append(pdu, []byte(smppBind.AddressRange)...) //address_range
	pdu = append(pdu, 0)                                //endstring 0

	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
