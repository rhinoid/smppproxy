package protocol

type SmppDeliverSMResponse struct {
	smppStatus     uint32
	sequenceNumber uint32
}

func (smppDeliverSMResponse *SmppDeliverSMResponse) Validate() bool {
	return true
}

func (smppDeliverSMResponse *SmppDeliverSMResponse) Serialize() []byte {
	pdu := make([]byte, 0, 16)

	pdu = append(pdu, createByteArrayFromInteger(SmppCommandIdDeliverSMResp)...)           //command_id
	pdu = append(pdu, 0, 0, 0, 0)                                                          //command_status
	pdu = append(pdu, createByteArrayFromInteger(smppDeliverSMResponse.sequenceNumber)...) //seq number
	pdu = append(pdu, 0)                                                                   //messageid (must be NULL)

	//prepend with length
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
