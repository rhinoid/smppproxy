package protocol

type SmppResponse struct {
	smppCommand    uint32 //see constants in PDU.go
	smppStatus     uint32
	sequenceNumber uint32
}

func (smppResponse *SmppResponse) Validate() bool {
	//this PDU is so simple, there is nothing to validate
	return true
}

func (smppResponse *SmppResponse) Serialize() []byte {
	pdu := make([]byte, 0, 16)

	pdu = append(pdu, createByteArrayFromInteger(smppResponse.smppCommand)...)    //command_id
	pdu = append(pdu, 0, 0, 0, 0)                                                 //command_status
	pdu = append(pdu, createByteArrayFromInteger(smppResponse.sequenceNumber)...) //seq number

	//prepend with length
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
