package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"strconv"
)

type PassiveTransmitter struct {
	ProtocolBase
}

func (protocol *PassiveTransmitter) SocketConnected() {
	protocol.changeState(ProtocolStateUnbound)
}

func (protocol *PassiveTransmitter) HandleResponsePDU(pdu []byte) {
	//We don't specifically expect any PDU responses in the passive transmitter mode
	l4g.Error("<%s>Received response PDU for a PDU we didn't send in passivetransmitter mode!?! Let's reconnect", protocol.logTag)
	protocol.forceReconnect()

	//TODO: act on error codes
}

func (protocol *PassiveTransmitter) HandleIncomingPDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	sequenceNumber := utils.ExtractIntegerFromArray(pdu[12:16])

	//wait for an incoming bind
	switch commandId {
	case SmppCommandIdBindTransmitter:
		if protocol.state == ProtocolStateUnbound {
			protocol.changeState(ProtocolStateBound)
			l4g.Info("<%s>Connection Bound. Ready for transmit", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Bound PDU, while we are already bound? Ignoring this", protocol.logTag)
		}
		protocol.sendBindResponse(sequenceNumber, commandId)
		break
	case SmppCommandIdSubmitSM:
		//Request unique local id to send back to mads
		l4g.Debug("<%s>Requesting uniqueID. Wait for response", protocol.logTag)
		returnIdChannel := make(chan int)
		protocol.databaseCounters.GetID(returnIdChannel)

		//Wait indefinately for unique ID to be created
		uniqueId := <-returnIdChannel
		pduResponseId := "proxid" + strconv.Itoa(uniqueId)
		l4g.Debug("<%s>ID received. Sending response with the uniqueid: %s", protocol.logTag, pduResponseId)

		l4g.Info("<%s>Received submit_sm PDU", protocol.logTag)
		protocol.sendSubmitSMReponse(sequenceNumber, pduResponseId)

		//We now have to store the PDU in the database, until it it picked up by the activetransmitter
		//To let that database know the ID we used we'll store it for now in the PDU in a field which we'll need
		//to overwrite anywayz (The seqnr field in the header)
		uniqueIdArray := createByteArrayFromInteger(uint32(uniqueId))
		pdu[12] = uniqueIdArray[0]
		pdu[13] = uniqueIdArray[1]
		pdu[14] = uniqueIdArray[2]
		pdu[15] = uniqueIdArray[3]

		//store the PDU in the database
		protocol.OutPDUStorageChannel <- pdu
		break
	case SmppCommandIdDataSM:
		//Request unique local id to send back to mads
		l4g.Debug("<%s>Requesting uniqueID. Wait for response", protocol.logTag)
		returnIdChannel := make(chan int)
		protocol.databaseCounters.GetID(returnIdChannel)

		//Wait indefinately for unique ID to be created
		uniqueId := <-returnIdChannel
		pduResponseId := "proxid" + strconv.Itoa(uniqueId)
		l4g.Debug("<%s>ID received. Sending response with the uniqueid: %s", protocol.logTag, pduResponseId)

		l4g.Info("<%s>Received data_sm PDU", protocol.logTag)
		protocol.sendDataSMReponse(sequenceNumber, pduResponseId)

		//We now have to store the PDU in the database, until it it picked up by the activetransmitter
		//To let that database know the ID we used we'll store it for now in the PDU in a field which we'll need
		//to overwrite anywayz (The seqnr field in the header)
		uniqueIdArray := createByteArrayFromInteger(uint32(uniqueId))
		pdu[12] = uniqueIdArray[0]
		pdu[13] = uniqueIdArray[1]
		pdu[14] = uniqueIdArray[2]
		pdu[15] = uniqueIdArray[3]

		//store the PDU in the database
		protocol.OutPDUStorageChannel <- pdu
		break
	case SmppCommandIdUnbind:
		if protocol.state == ProtocolStateBound {
			l4g.Info("<%s>Received Unbind PDU", protocol.logTag)
		} else {
			l4g.Warn("<%s>Received Unbind PDU, while we are not bound? Ignoring this", protocol.logTag)
		}
		protocol.sendResponse(sequenceNumber, commandId)
		//given an unbound state, we might as well flush the socket
		protocol.forceReconnect()
		break
	default:
		//Unrecognized or unpexpected PDU
		l4g.Error("<%s>Received PDU (%d(%s)) which we should not receive. Closing connection", protocol.logTag, commandId, commandIdToString(commandId))
		protocol.forceReconnect()
		break
	}

}

func (protocol *PassiveTransmitter) InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte {
	return pdu
}

//todo recieve windowing

//todo check if we send responses everywhere
