package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
)

//states the protocol goes through
const (
	ProtocolStateUnconnected = 1 //unconnected
	ProtocolStateUnbound     = 2 //connected but not bound yet
	ProtocolStateBound       = 3 //bound and connected. ready to transmit/receive
)

//The interface any of the protocols has to abide to
type ActiveProtocolInterface interface {
	//InitChannels: a method which initializes the incoming channels
	InitChannels(logTag string)

	//Start: a method which starts the protocol
	Start(config *config.SmppProtocolConfig, outPDUTransportChannel chan []byte, outPDUStorageChannel chan utils.PDUBytes, outResetTransportChannel chan int, protocolHandler ActiveProtocolHandlerInterface)

	GetInboundStorageChannel() chan utils.PDUBytes
	GetInboundTransmitChannel() chan []byte
	GetInboundResetTransportChannel() chan int

	SetOutboundStorageChannel(outPDUStorageChannel chan utils.PDUBytes)
	SetOutboundTransportChannel(outPDUTransportChannel chan []byte)
	SetOutboundTransportResetChannel(outResetTransportChannel chan int)
}

//The interface with the specifics to handle the incoming protocol data
type ActiveProtocolHandlerInterface interface {
	HandleIncomingPDU(pdu []byte)
	HandleResponsePDU(pdu []byte)
	SocketConnected()
	InspectPduBeforeTransmit(pdu []byte, seqNr uint32) []byte
}
