package protocol

type SmppBindResponse struct {
	smppCommand    uint32 //see constants in PDU.go
	smppStatus     uint32
	sequenceNumber uint32
}

func (smppBindResponse *SmppBindResponse) Validate() bool {
	//this PDU is so simple, there is nothing to validate
	return true
}

func (smppBindResponse *SmppBindResponse) Serialize() []byte {
	pdu := make([]byte, 0, 16)

	pdu = append(pdu, createByteArrayFromInteger(smppBindResponse.smppCommand)...)    //command_id
	pdu = append(pdu, 0, 0, 0, 0)                                                     //command_status
	pdu = append(pdu, createByteArrayFromInteger(smppBindResponse.sequenceNumber)...) //seq number
	pdu = append(pdu, []byte("MADS")...)                                              //system_type
	pdu = append(pdu, 0)                                                              //endstring 0

	//prepend with length
	lengthPdu := createByteArrayFromInteger(uint32(len(pdu) + 4))
	pdu = append(lengthPdu, pdu...)
	return pdu
}
