package protocol

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/database"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"fmt"
	"os"
	"sync"
	"time"
)

const maxStateHistory = 5
const protocolNonHammerTime = 3

type StateHistoryDebug struct {
	state     int
	timeStamp time.Time
}

type ProtocolBase struct {
	// bytes which are received and which need to be sent. Each byte slice must contain one full PDU
	InPDUStorageChannel      chan utils.PDUBytes //PDU's which need to be sent (from the database)
	InPDUTransportChannel    chan []byte         //PDU's which were received from the transport layer
	InResetTransportChannel  chan int            //Reset channel on which the transport can indicate we need to reconnect the protocol
	OutPDUStorageChannel     chan utils.PDUBytes //PDU's which need to be stored in the database
	OutPDUTransportChannel   chan []byte         //PDU's which need to be sent to the transport
	OutResetTransportChannel chan int            //Reset channel on which we can signal the transport it needs to reconnect

	//internal stuff
	pduLastTimeStamp          time.Time //timestamp last time something happened
	lastPongReceivedTimestamp time.Time //timestamp when we got the last enquire_link response (ping/pong)
	lastPongSentTimestamp     time.Time //timestamp when we sent the last enquire_link response (ping/pong)
	state                     int       //Use the ProtocolState consts

	pduNumber     uint32     //number of the next PDU to send/receive
	pduNumberLock sync.Mutex //lock to make sure not 2 coroutines are manipulating this number at the same time

	sentPDUNumbers       map[uint32]time.Time //timestap of sent pdu's. To calculate windowing and pdu timeouts
	sentPDUNumbersRWLock sync.RWMutex         //mutex to make concurrent access possible

	connctedButNotBoundTimeout time.Time //how long since a connection should we wait for bind, until we close the connection
	waitForWindowTimeout       time.Time //how long haven't we recieved any responses anymore, and is the send window full

	//logging / debugging
	logTag             string
	stateHistory       []StateHistoryDebug
	speedCalcOutBuffer []int //used to calculate the average speed
	speedCalcInBuffer  []int //used to calculate the average speed

	//easy access to the configuration
	protocolConfig *config.SmppProtocolConfig

	//actual implementation of the protocol specifics
	protocolHandler ActiveProtocolHandlerInterface

	//database connection for counters (for rewriting delivery notification id's)
	databaseCounters database.RedisCountersInterface
}

func (protocol *ProtocolBase) IsBoundDescription() string {
	if protocol.state == ProtocolStateBound {
		return "true"
	} else {
		return "false"
	}
}

func (protocol *ProtocolBase) OutBoundSpeedDescription() string {
	second := time.Now().Second() % 60
	averageOutRecent := 0
	for i := 0; i < 6; i++ {
		averageOutRecent += protocol.speedCalcOutBuffer[(second-1-i+60)%60]
	}
	return fmt.Sprintf("%d", averageOutRecent/6)
}

func (protocol *ProtocolBase) InBoundSpeedDescription() string {
	second := time.Now().Second() % 60
	averageInRecent := 0
	for i := 0; i < 6; i++ {
		averageInRecent += protocol.speedCalcInBuffer[(second-1-i+60)%60]
	}
	return fmt.Sprintf("%d", averageInRecent/6)
}

func (protocol *ProtocolBase) Description() string {
	response := ""

	//calc average speeds over last 5 seconds and last minute
	second := time.Now().Second() % 60
	averageOutRecent := 0
	averageInRecent := 0
	averageOutMinute := 0
	averageInMinute := 0
	for i := 0; i < 6; i++ {
		averageOutRecent += protocol.speedCalcOutBuffer[(second-1-i+60)%60]
		averageInRecent += protocol.speedCalcInBuffer[(second-1-i+60)%60]
	}
	for i := 0; i < 60; i++ {
		averageOutMinute += protocol.speedCalcOutBuffer[i]
		averageInMinute += protocol.speedCalcInBuffer[i]
	}
	response += fmt.Sprintf("curspeed (out:%.2f,in:%.2f) last minute(out:%.2f,in:%.2f)<br>", float32(averageOutRecent)/6.0, float32(averageInRecent)/6.0, float32(averageOutMinute)/60.0, float32(averageInMinute)/60.0)

	first := true
	for i := len(protocol.stateHistory) - 1; i >= 0; i-- {
		if !first {
			response += "<font size=-2>"
		}
		stateHistoricDataPoint := protocol.stateHistory[i]
		response += protocolStateToString(stateHistoricDataPoint.state)

		response += fmt.Sprintf("(since %d seconds)<br>", time.Now().Sub(stateHistoricDataPoint.timeStamp)/time.Second)

		if !first {
			response += "</font>"
		}
		first = false
	}

	return response
}

func protocolStateToString(state int) string {
	switch state {
	case ProtocolStateUnconnected:
		return "Unconnected"
	case ProtocolStateUnbound:
		return "Unbound"
	case ProtocolStateBound:
		return "Bound"
	}

	return "Unknown"
}

func (protocol *ProtocolBase) InitChannels(logTag string) {
	protocol.InPDUStorageChannel = make(chan utils.PDUBytes)
	protocol.InPDUTransportChannel = make(chan []byte)
	protocol.InResetTransportChannel = make(chan int)
	protocol.logTag = logTag
	protocol.state = ProtocolStateUnconnected
	protocol.stateHistory = make([]StateHistoryDebug, 0)
	protocol.speedCalcOutBuffer = make([]int, 60)
	protocol.speedCalcInBuffer = make([]int, 60)
}

func (protocol *ProtocolBase) Start(config *config.SmppProtocolConfig, protocolHandler ActiveProtocolHandlerInterface) {
	protocol.protocolConfig = config
	l4g.Info("<%s>Initialize protocol", protocol.logTag)

	protocol.protocolHandler = protocolHandler
	protocol.state = ProtocolStateUnconnected

	go protocol.transmitterResetGoRoutine()
	go protocol.transmitterSendGoRoutine()
	go protocol.transmitterRcvGoRoutine()
	go protocol.clearSpeedBuffersGoRoutine()
}

func (protocol *ProtocolBase) GetInboundStorageChannel() chan utils.PDUBytes {
	return protocol.InPDUStorageChannel
}

func (protocol *ProtocolBase) GetInboundTransmitChannel() chan []byte {
	return protocol.InPDUTransportChannel
}

func (protocol *ProtocolBase) GetInboundResetTransportChannel() chan int {
	return protocol.InResetTransportChannel
}

func (protocol *ProtocolBase) SetOutboundStorageChannel(outPDUStorageChannel chan utils.PDUBytes) {
	protocol.OutPDUStorageChannel = outPDUStorageChannel
}

func (protocol *ProtocolBase) SetOutboundTransportChannel(outPDUTransportChannel chan []byte) {
	protocol.OutPDUTransportChannel = outPDUTransportChannel
}

func (protocol *ProtocolBase) SetOutboundTransportResetChannel(outResetTransportChannel chan int) {
	protocol.OutResetTransportChannel = outResetTransportChannel
}

func (protocol *ProtocolBase) SetDatabaseCounters(databaseCounters database.RedisCountersInterface) {
	protocol.databaseCounters = databaseCounters
}

func (protocol *ProtocolBase) clearSpeedBuffersGoRoutine() {
	for {
		second := time.Now().Second() % 60
		protocol.speedCalcOutBuffer[(second+1)%60] = 0
		protocol.speedCalcInBuffer[(second+1)%60] = 0

		time.Sleep(time.Second) //only clear the speed buffers once a second
	}
}

func (protocol *ProtocolBase) transmitterResetGoRoutine() {
	for {
		select {
		case connectedOrNot := <-protocol.InResetTransportChannel:
			//if we received an unconnected or error state, then we have to rebind
			l4g.Debug("<%s>received %d and currentstate = %s", protocol.logTag, connectedOrNot, protocolStateToString(protocol.state))
			if connectedOrNot == 0 {
				if protocol.state != ProtocolStateUnconnected {
					protocol.changeState(ProtocolStateUnconnected)
					l4g.Info("<%s>Socket went to unconnected state. Need to rebind", protocol.logTag)
				}
			} else if connectedOrNot == 1 {
				if protocol.state == ProtocolStateUnconnected {
					l4g.Info("<%s>Socket went to connected state", protocol.logTag)
					protocol.pduNumberLock.Lock()
					protocol.pduNumber = 0
					protocol.pduNumberLock.Unlock()

					protocol.protocolHandler.SocketConnected()
				}
			}

		case <-time.After(5 * time.Second):
		}
	}
}

func (protocol *ProtocolBase) transmitterSendGoRoutine() {
	for {
		l4g.Debug("<%s>protocol transmittersendgo routine. Current state = %s", protocol.logTag, protocolStateToString(protocol.state))
		//now depending on the state of the protocol, do the correct thing
		switch protocol.state {
		case ProtocolStateUnconnected:
			time.Sleep(protocolNonHammerTime * time.Second) //sleep to not keep on using resources
			break
		case ProtocolStateUnbound:
			//keep waiting until the bound has been been replied to
			time.Sleep(protocolNonHammerTime * time.Second) //sleep to not keep on using resources

			//check for some sensible timeout
			timePassed := time.Time.Sub(time.Now(), protocol.connctedButNotBoundTimeout)
			if timePassed > (time.Duration(protocol.protocolConfig.ConnectButNotBoundTimeOutInSeconds) * time.Second) {
				l4g.Error("<%s>Connection was made, but we never bound (protocolConfig.ConnectButNotBoundTimeOutInSeconds reached). Let's reconnect", protocol.logTag)
				protocol.forceReconnect()

			}

			break
		case ProtocolStateBound:
			//Is the smpp window full?
			protocol.sentPDUNumbersRWLock.RLock()
			pdusInFlight := len(protocol.sentPDUNumbers)
			protocol.sentPDUNumbersRWLock.RUnlock()
			if pdusInFlight > int(protocol.protocolConfig.WindowSize) {
				//then wait a bit
				time.Sleep(time.Duration(protocol.protocolConfig.SleepForWindowFullWaitInMS) * time.Millisecond)

				//TODO:  if answers never come, close connection
				noResponseRecievedTime := time.Time.Sub(time.Now(), protocol.waitForWindowTimeout)
				if noResponseRecievedTime > (time.Duration(protocol.protocolConfig.MaxTimeToWaitForPDUResponseInSeconds) * time.Second) {
					l4g.Error("<%s>SMPP Windowing full timeout reached (No response PDUs anynore). Closing connection", protocol.logTag)
					protocol.forceReconnect()
				}

			} else {
				//are we sending too fast?
				now := time.Now()
				second := now.Second() % 60
				pdusAlreadySentThisSecond := protocol.speedCalcOutBuffer[second]
				if pdusAlreadySentThisSecond >= int(protocol.protocolConfig.FrequencyCap) {
					//wait until the second is full
					fractionalTime := time.Duration(now.Nanosecond())
					l4g.Debug("<%s>freqcap reached....sleeping", protocol.logTag)
					time.Sleep(time.Second - fractionalTime)
				}

				//check if there is anything to send
				select {
				case pduToSend := <-protocol.InPDUStorageChannel:
					protocol.sendPDU(pduToSend)
				case <-time.After(5 * time.Second):
				}

				protocol.checkToSendPing()
				protocol.waitForWindowTimeout = time.Now()

			}
			break
		default:
			time.Sleep(protocolNonHammerTime * time.Second) //sleep to not keep on using resources
			break
		}
	}
}

func (protocol *ProtocolBase) transmitterRcvGoRoutine() {
	var currentReceiveBuffer []byte = nil
	for {
		l4g.Debug("<%s>protocol transmitterrcvgo routine. Current state = %s", protocol.logTag, protocolStateToString(protocol.state))

		//Independent of the protocolsocketstate always read the incoming data (which can still have valid pdu's in it from the previous connection)
		dataReceived := false
		select {
		case newData := <-protocol.InPDUTransportChannel:
			if currentReceiveBuffer == nil {
				//start of 1 or more pdu's
				currentReceiveBuffer = newData
			} else {
				//extra data for the pdu's
				currentReceiveBuffer = append(currentReceiveBuffer, newData...)
			}
			dataReceived = true
		case <-time.After(5 * time.Second):
		}

		//if the state is unconnected, then ignore any loose bytes from previous times
		if protocol.state == ProtocolStateUnconnected {
			dataReceived = false
			currentReceiveBuffer = nil
		}

		if dataReceived {
			l4g.Debug("<%s>data in the recieve buffer. length: %d", protocol.logTag, len(currentReceiveBuffer))
			for { //extract all pdu's in the current buffer
				firstPDU := extractFirstPDU(currentReceiveBuffer)
				if firstPDU != nil {
					//parse pdu
					protocol.handlePDU(firstPDU)

					//remove PDU from current receive buffer
					currentReceiveBuffer = removeFirstPDU(currentReceiveBuffer)
				} else {
					//no valid pdus anymore, continue reading at the socket
					break
				}
			}
		}

		//sanity check if the PDU doesn't seem bogus (basically we lost sync and need to reconnect)
		if currentReceiveBuffer != nil && len(currentReceiveBuffer) > 16 {
			pduLength := utils.ExtractIntegerFromArray(currentReceiveBuffer)
			if pduLength > protocol.protocolConfig.MaxAllowedPDUSize {
				l4g.Error("<%s>Received a PDU with a size of: %d, which exceeded the maximum of %d. Forcing a reconnect", protocol.logTag, pduLength, protocol.protocolConfig.MaxAllowedPDUSize)
				protocol.forceReconnect()
				currentReceiveBuffer = nil
			}
		}
	}
}

func (protocol *ProtocolBase) initProtocol() {
	protocol.pduLastTimeStamp = time.Now()
	protocol.lastPongReceivedTimestamp = time.Now()
	protocol.pduNumberLock.Lock()
	protocol.pduNumber = 0
	protocol.pduNumberLock.Unlock()
	protocol.sentPDUNumbers = make(map[uint32]time.Time)
}

func (protocol *ProtocolBase) changeState(newState int) {
	var mutex = &sync.Mutex{}

	//do something when the state changes
	switch newState {
	case ProtocolStateUnconnected:
		protocol.pduNumberLock.Lock()
		protocol.pduNumber = 0
		protocol.pduNumberLock.Unlock()
		break
	case ProtocolStateUnbound:
		//reset the reset channel
		protocol.emptyResetChannel()
		protocol.initProtocol()
		protocol.connctedButNotBoundTimeout = time.Now()
		break
	case ProtocolStateBound:
		protocol.waitForWindowTimeout = time.Now()
		break
	default:
		break
	}

	mutex.Lock()
	protocol.state = newState
	mutex.Unlock()

	//for debugging, push the state history
	stateHistoricDataPoint := StateHistoryDebug{state: newState, timeStamp: time.Now()}
	protocol.stateHistory = append(protocol.stateHistory, stateHistoricDataPoint)
	if len(protocol.stateHistory) > maxStateHistory {
		//remove first element
		protocol.stateHistory = append(protocol.stateHistory[:0], protocol.stateHistory[1:]...)
	}
}

//send the appropriate bind PDU to the server
func (protocol *ProtocolBase) sendBind(bindMode int) {
	var bindPDU SmppBind

	configuration := protocol.protocolConfig

	bindPDU.BindMode = bindMode
	bindPDU.SystemId = configuration.SystemId
	bindPDU.Password = configuration.Password
	bindPDU.SystemType = configuration.SystemType
	bindPDU.AddrTon = configuration.AddrTon
	bindPDU.AddrNpi = configuration.AddrNpi
	bindPDU.AddressRange = configuration.AddressRange
	bindPDU.InterfaceVersion = configuration.SmppVersion

	validPDU := bindPDU.Validate()
	if !validPDU {
		l4g.Critical("<%s>The Bind PDU as transmitter we need to send is not valid. Please check all the fields (also for length)", protocol.logTag)
		time.Sleep(time.Second) //flush l4g buffers
		os.Exit(-1)

		//LOG error
	}
	rawPDU := bindPDU.Serialize()
	protocol.sendPDU(rawPDU)
}

func (protocol *ProtocolBase) sendPDU(pdu []byte) {
	//explicitly set the sequence number of the pdu to send
	protocol.pduNumberLock.Lock()
	pduNumber := protocol.pduNumber
	protocol.pduNumber = protocol.pduNumber + 1
	protocol.pduNumberLock.Unlock()
	sequenceNumber := createByteArrayFromInteger(pduNumber)

	pdu = protocol.protocolHandler.InspectPduBeforeTransmit(pdu, pduNumber)

	pdu[12] = sequenceNumber[0]
	pdu[13] = sequenceNumber[1]
	pdu[14] = sequenceNumber[2]
	pdu[15] = sequenceNumber[3]
	protocol.OutPDUTransportChannel <- pdu

	protocol.sentPDUNumbersRWLock.Lock()
	protocol.sentPDUNumbers[pduNumber] = time.Now() //add PDU timestamp
	protocol.sentPDUNumbersRWLock.Unlock()

	protocol.pduLastTimeStamp = time.Now()

	second := time.Now().Second() % 60
	protocol.speedCalcOutBuffer[second]++
}

func (protocol *ProtocolBase) forceReconnect() {
	protocol.OutResetTransportChannel <- 1
	protocol.changeState(ProtocolStateUnconnected)
}

func (protocol *ProtocolBase) checkToSendPing() {
	timePassed := time.Time.Sub(time.Now(), protocol.lastPongSentTimestamp)
	if timePassed > (time.Duration(protocol.protocolConfig.PingTimeInSeconds) * time.Second) {
		protocol.sendPing()
	}

}

func (protocol *ProtocolBase) sendPing() {
	protocol.lastPongSentTimestamp = time.Now()

	enquireLinkPDU := SmppEnquireLink{}

	validPDU := enquireLinkPDU.Validate()
	if !validPDU {
		l4g.Critical("<%s>The EnquireLink PDU as transmitter we need to send is not valid. Please check all the fields (also for length)", protocol.logTag)
		time.Sleep(time.Second) //flush l4g buffers
		os.Exit(-1)

		//LOG error
	}
	rawPDU := enquireLinkPDU.Serialize()
	protocol.sendPDU(rawPDU)
}

func (protocol *ProtocolBase) sendResponse(sequenceNumber uint32, smppCommand uint32) {
	responsePDU := SmppResponse{}
	responsePDU.smppCommand = smppCommand | 0x80000000
	responsePDU.sequenceNumber = sequenceNumber
	rawPDU := responsePDU.Serialize()
	protocol.OutPDUTransportChannel <- rawPDU
}

func (protocol *ProtocolBase) emptyResetChannel() {
	for {
		select {
		case _ = <-protocol.InResetTransportChannel:
		default:
			return
		}
	}
}

func (protocol *ProtocolBase) handlePDU(pdu []byte) {
	commandId := utils.ExtractIntegerFromArray(pdu[4:8])
	commandStatus := utils.ExtractIntegerFromArray(pdu[8:12])
	sequenceNumber := utils.ExtractIntegerFromArray(pdu[12:16])

	l4g.Debug("<%s>incoming PDU with commandId %s", protocol.logTag, commandIdToString(commandId))
	//it doesn't matter what we receive. Any valid pdu will reset the last time we recieved the pong result timestamp
	protocol.lastPongReceivedTimestamp = time.Now()

	
	//check PDU type
	if (commandId & 0x80000000) == 0x80000000 {
		//Response PDU?

		//check if the pdu response we got actually belonged to something we sent before

		protocol.sentPDUNumbersRWLock.RLock()
		_, ok := protocol.sentPDUNumbers[sequenceNumber]
		protocol.sentPDUNumbersRWLock.RUnlock()
		if !ok {
			l4g.Error("<%s>Just received a PDU response for which we haven't sent anything (yet). Let's reconnect", protocol.logTag)
			protocol.forceReconnect()
			return
		}

		//remove entry from the PDUmap
		protocol.sentPDUNumbersRWLock.Lock()
		delete(protocol.sentPDUNumbers, sequenceNumber)
		protocol.sentPDUNumbersRWLock.Unlock()

		//Check if we received a throttling error
		if commandStatus == SmppCommandStatusThrottlingEsmeRThrottled {
			//in essence receiving a throttle error is nothing more than halting this goroutine for X seconds
			time.Sleep(time.Duration(protocol.protocolConfig.ThrottlingWaitTimeInSeconds) * time.Second) //sleep to not keep on using resources
		}

		//check if it is the enquirelink response (ping)
		if commandId == SmppCommandIdEnquireLink|0x80000000 {
			// do nothing
		} else {
			//do the protocol specific handling
			protocol.protocolHandler.HandleResponsePDU(pdu)
		}

	} else {
		//Command PDU
		second := time.Now().Second() % 60
		protocol.speedCalcInBuffer[second]++

		//check for inquire link request from the other side
		if commandId == SmppCommandIdEnquireLink {
			//respond with the enquirelink response (pong)
			protocol.sendResponse(sequenceNumber, commandId)
		} else {
			protocol.protocolHandler.HandleIncomingPDU(pdu)
		}
	}
}

func (protocol *ProtocolBase) sendBindResponse(sequenceNumber uint32, smppCommand uint32) {
	responsePDU := SmppBindResponse{}
	responsePDU.smppCommand = smppCommand | 0x80000000
	responsePDU.sequenceNumber = sequenceNumber
	if !responsePDU.Validate() {
		l4g.Error("<%s>SubmitSMResp PDU is not valid", protocol.logTag)
		//This should never give an error, unless there is some kind of programming error
	}
	rawPDU := responsePDU.Serialize()
	protocol.OutPDUTransportChannel <- rawPDU
}

func (protocol *ProtocolBase) sendSubmitSMReponse(sequenceNumber uint32, messageId string) {
	responsePDU := SmppSubmitSMResponse{}
	responsePDU.sequenceNumber = sequenceNumber
	responsePDU.messageId = messageId
	if !responsePDU.Validate() {
		l4g.Error("<%s>SubmitSMResp PDU is not valid", protocol.logTag)
		//This should never give an error, unless there is some kind of programming error
	}
	rawPDU := responsePDU.Serialize()
	protocol.OutPDUTransportChannel <- rawPDU
}

func (protocol *ProtocolBase) sendDataSMReponse(sequenceNumber uint32, messageId string) {
	responsePDU := SmppDataSMResponse{}
	responsePDU.sequenceNumber = sequenceNumber
	responsePDU.messageId = messageId
	if !responsePDU.Validate() {
		l4g.Error("<%s>DataSMResp PDU is not valid", protocol.logTag)
		//This should never give an error, unless there is some kind of programming error
	}
	rawPDU := responsePDU.Serialize()
	protocol.OutPDUTransportChannel <- rawPDU
}

func (protocol *ProtocolBase) sendDeliverSMReponse(sequenceNumber uint32) {
	responsePDU := SmppDeliverSMResponse{}
	responsePDU.sequenceNumber = sequenceNumber
	if !responsePDU.Validate() {
		l4g.Error("<%s>DeliverSMResp PDU is not valid", protocol.logTag)
		//This should never give an error, unless there is some kind of programming error
	}
	rawPDU := responsePDU.Serialize()
	protocol.OutPDUTransportChannel <- rawPDU
}
