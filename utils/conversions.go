package utils

import "errors"

func ExtractIntegerFromArray(data []byte) uint32 {
	return (uint32(data[0]) << 24) | (uint32(data[1]) << 16) | (uint32(data[2]) << 8) | uint32(data[3])
}

func ExtractUint16FromArray(data []byte) uint16 {
	return (uint16(data[0]) << 8) | uint16(data[1])
}

func ExtractStringFromArray(data []byte) (value string, err error) {
	//find trailing zero
	zeroIndex := 0
	for zeroIndex < len(data) && data[zeroIndex] != 0 {
		zeroIndex += 1
	}

	//string has no ending zero? (or buffer too short)
	if zeroIndex >= len(data) {
		return "", errors.New("string could not be parsed out of buffer. Buffer too short")
	}

	return string(data[0:zeroIndex]), nil
}

func ExtractFixedLengthStringFromArray(data []byte, length int) (value string, err error) {
	if length > len(data) {
		return "", errors.New("fixed length string could not be parsed out of buffer. Buffer too short")
	}

	return string(data[0:length]), nil
}
