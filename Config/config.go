package config

import (
	l4g "code.google.com/p/log4go"
	"encoding/json"
	"os"
	"time"
)

type CountersConfig struct {
	DatabaseConfig          DatabaseConfig
	CounterWrapAroundMaxInt int //recommended: 24bit = 16,777,215
}

type WebServerConfig struct {
	Port uint32 // Port nr on which we listen and display live proxy stats/ debug info
}

type TransportLayerConfig struct {
	Server                string //ip of the server we have to connect to: e.g. 127.0.0.1 (or the ip of the local listen socket)
	Port                  uint16 //port where we listen on OR the port the transmitter and reciever try to connect
	ReconnectAfterSeconds int    //If the connection with the provider is disconnected. After how many seconds should we retry?
}

type SmppProtocolConfig struct {
	SystemId     string //for now only relevant for the active connection. Any connection which tries to bind to the passive connection, we just accept
	Password     string
	SystemType   string
	AddrTon      uint8
	AddrNpi      uint8
	AddressRange string

	WindowSize                           uint8  //max pdu's allowed to be sent to the provider, before we receive the corresponding reply pdu
	SmppVersion                          uint8  //protocol version. should probably be 52 (hex 34... which stands for v3.4)
	MaxTimeToWaitForPDUResponseInSeconds uint32 //if a PDUresponse is not received in X seconds, we'll disconnect
	ConnectButNotBoundTimeOutInSeconds   uint32 //After a socket connects, how long for a bound state to be reached for we disconnect?
	ThrottlingWaitTimeInSeconds          uint32 //If a throttle commandStatus is recieved. How many seconds should we wait. (some specs say 15 seconds)
	MaxAllowedPDUSize                    uint32 //How big can a PDU be maximally? (should be something like 32000)
	PingTimeInSeconds                    uint32 //after how many seconds should we send a ping? (should typically be 60)
	FrequencyCap                         uint32 //How many pdu's per second can we receive? (0=nocap)

	//expert settings
	SleepForWindowFullWaitInMS uint32 //How many ms do we sleep before trying again to see if the PDU sent window is empty enough to send PDU's again(recommended: 50)
        ConvertDeliveryIdFromHexToDec        bool   //If the delivery notificationID is sent back as a decimal string instead of hex, convert it
}

type DatabaseConfig struct {
	RedisServer                  string //database location to temporarily store pdu's (e.g.: 127.0.0.1:6379 )
	RedisPassword                string //database password
	KeyName                      string //unique 'key' into which PDU's are written or read
	MaxBufferedPDUs              uint32 //0 = unlimited
	BlockingReadTimeOutInSeconds uint32 //When reading data from redis(blocking) after how many seconds should we timeout (recommended: 30)
	// Do note that after the timeout, the read will be retried (indefinately)
	MaxTimeConnectionIdleInSeconds uint32 //When a connection to the redis db is not used, after how many seconds can it be closed (recommended 240)
	MaxIdleConnections             uint32 //How many connections to the redis db can there be idle at the same time (recommended 3)
}

type ReceiverConfig struct {
	DatabaseConfig     DatabaseConfig
	SmppProtocolConfig SmppProtocolConfig
}

type TransmitterConfig struct {
	DatabaseConfig     DatabaseConfig
	SmppProtocolConfig SmppProtocolConfig
}

type ActiveConnectionConfig struct {
	TransceiverMode      bool // true = the proxy will try to connect as a transceiver
	NumberOfConnections  uint32
	TransmitterConfig    TransmitterConfig
	ReceiverConfig       ReceiverConfig
	TransportLayerConfig TransportLayerConfig
}

type PassiveConnectionConfig struct {
	TransmitterConfig    TransmitterConfig
	ReceiverConfig       ReceiverConfig
	TransportLayerConfig TransportLayerConfig
}

type Configuration struct {
	ActiveConnection  ActiveConnectionConfig  // The connection to the provider (we actively try to connect)
	PassiveConnection PassiveConnectionConfig // The connection to the local SMPP server (we passively wait for incoming connections)
	CountersConfig    CountersConfig          // The counters needed to store and map del receipts
	WebServerConfig   WebServerConfig         // Info on the local webserver
}

func ParseConfigFile(filename string) *Configuration {
	l4g.Info("<Config>Parsing config file")
	file, _ := os.Open(filename)
	decoder := json.NewDecoder(file)
	config := &Configuration{}
	err := decoder.Decode(&config)
	if err != nil {
		l4g.Critical("<Config>Fatal error: %s", err.Error())
		time.Sleep(time.Second) //flush l4g buffers
		os.Exit(1)
	}

	l4g.Info("<Config>The following config will be used:")
	l4g.Info("<Config>%#v\r\n", config)
	return config
}
