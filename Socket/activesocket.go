package socket

import (
	l4g "code.google.com/p/log4go"
	"net"
	"strconv"
	"time"
)

type ActiveSocket struct {
	SocketBase
}

//The sole purpose of this method is to keep the socket open, and reconnect after errors
func (connection *ActiveSocket) connectionGoRoutine() {
	var err error
	var server = connection.transportLayerConfig.Server + ":" + strconv.Itoa(int(connection.transportLayerConfig.Port))
	for {
		l4g.Debug("<%s>sendgoroutine in state: %s", connection.logTag, socketStateToString(connection.state))

		switch connection.state {
		case SocketStateUnconnected:
			l4g.Info("<%s>Trying to connect to: %s", connection.logTag, server)
			connection.socket, err = net.Dial("tcp", server)
			if err != nil {
				connection.socket = nil
				connection.socketLastTimeStamp = time.Now()
				connection.changeState(SocketStateReconnect)
				//LOGGING!
				l4g.Error("<%s>Error trying to connect to %s: %s", connection.logTag, server, err.Error())
			} else {
				//empty the reset channel
				connection.emptyResetChannel()

				//and set the state to connected
				connection.changeState(SocketStateConnected)
				l4g.Info("<%s>Socket connected", connection.logTag)
			}
			break
		case SocketStateError:
			l4g.Debug("<%s>Closing socket", connection.logTag)
			err := connection.socket.Close()
			if err != nil {
				l4g.Warn("<%s>Error trying to close the socket %s: %s", connection.logTag, server, err.Error())
			}
			connection.socketLastTimeStamp = time.Now()
			connection.changeState(SocketStateReconnect)
			break
		case SocketStateReconnect:
			if time.Duration.Seconds(time.Time.Sub(time.Now(), connection.socketLastTimeStamp)) > float64(connection.transportLayerConfig.ReconnectAfterSeconds) {
				connection.changeState(SocketStateUnconnected)
				l4g.Info("<%s>'ReconnectAfterSeconds' elapsed. Going to retry connecting", connection.logTag)
			} else {
				//todo calc the exact reconnect time so to do the sleep exact instead of with 5 seconds increments
				time.Sleep(socketNonHammerTime * time.Second) //sleep to not keep on using resources
			}
			break
		default:
			time.Sleep(socketNonHammerTime * time.Second) //sleep to not keep on using resources
			break
		}

	}
}
