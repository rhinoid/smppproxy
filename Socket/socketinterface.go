package socket

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
)

const (
	SocketStateUnconnected = 1
	SocketStateConnected   = 2 //connected but not bound yet
	SocketStateError       = 3 //error... reconnect imminent
	SocketStateReconnect   = 4 //will reconnect ofter x milliseconds
)

type SocketInterface interface {
	//InitChannels: a method which initializes the incoming channels
	InitChannels(logTag string)

	//Start: a method which starts the protocol dataflow
	Start(config *config.TransportLayerConfig, connectionHandler SocketConnectorInterface)

	GetInboundResetChannel() chan int
	GetInboundDataChannel() chan []byte

	SetOutboundResetChannel(outResetChannel chan int)
	SetOutboundDataChannel(outRawChannel chan []byte)
}

//logic which actively or passively conects the socket
type SocketConnectorInterface interface {
	connectionGoRoutine()
}
