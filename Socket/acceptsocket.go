package socket

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"net"
	"os"
	"strconv"
	"time"
)

const ( //these are also defined in pdu.go, but we dont' want a dependency on the protocol part
	//since this socket should actually undertand nothing about the protocol
	SmppCommandIdBindReceiver    = 1
	SmppCommandIdBindTransmitter = 2
	SmppCommandIdBindTranceiver  = 9
)

type AcceptSocket struct {
	listenSocket net.Conn

	passiveRcvSocket *PassiveSocket
	passiveTrxSocket *PassiveSocket

	transportLayerConfig *config.TransportLayerConfig

	logTag string
}

func (connection *AcceptSocket) InitChannels(logTag string) {
	connection.logTag = logTag
}

func (connection *AcceptSocket) Start(config *config.TransportLayerConfig, passiveRcvSocket *PassiveSocket, passiveTrxSocket *PassiveSocket) {
	l4g.Info("<%s>Initialize", connection.logTag)

	connection.transportLayerConfig = config
	connection.passiveRcvSocket = passiveRcvSocket
	connection.passiveTrxSocket = passiveTrxSocket

	go connection.connectionGoRoutine()
}

func (connection *AcceptSocket) connectionGoRoutine() {
	var server = connection.transportLayerConfig.Server + ":" + strconv.Itoa(int(connection.transportLayerConfig.Port))
	l4g.Info("<%s>Listening on (host:port): %s", connection.logTag, server)
	for {

		laddr, err := net.ResolveTCPAddr("tcp", server)
		if nil != err {
			l4g.Critical("<%s>resolveaddr gave an error:", connection.logTag, err.Error())
			time.Sleep(time.Second) //flush l4g buffers
			os.Exit(1)
		}
		ln, err := net.ListenTCP("tcp", laddr)
		if err != nil {
			l4g.Critical("<%s>listen gave an error:", connection.logTag, err.Error())
			time.Sleep(time.Second) //flush l4g buffers
			os.Exit(1)
		}
		for {
			time.Sleep(socketNonHammerTime * time.Second) //in case of constant errors, we add a bit of a delay

			//we add a deadline so we can also check other stuff and don't want to block forever
			ln.SetDeadline(time.Now().Add(15 * time.Second))
			conn, err := ln.Accept() // this blocks until connection or error
			//filter out timouts which don't mean a thing
			logError := false
			if err != nil {
				logError = true
				switch err.(type) {
				case net.Error:
					{
						if err.(net.Error).Timeout() {
							logError = false //time out reached
						}
					}
				}
			}
			if logError {
				l4g.Info("<%s>Socket accept gave an error:", connection.logTag, err.Error())
			}
			if err != nil {
				//accept error? Then retry to accept again after slight delay
				time.Sleep(1 * time.Second)
				continue
			}

			//we have a connected socket... let's get the first 16 bytes, and then we know which mode is trying to connect to us (transmitter, receiver or transceiver)
			bytesReadBuffer := make([]byte, 16)
			conn.SetReadDeadline(time.Now().Add(60 * time.Second))
			bytesRead, err := conn.Read(bytesReadBuffer)
			l4g.Debug("<%s>Bytes read %d", connection.logTag, bytesRead)
			//filter out timouts which don't mean a thing
			if err != nil {
				switch err.(type) {
				case net.Error:
					{
						if err.(net.Error).Timeout() {
							err = nil //time out reached
						}
					}
				}
			}
			//check for actual errors
			if err != nil || bytesRead < 16 {
				l4g.Error("<%s>Error while trying to read the first bit of data on the acceptsocket. Err = %s", connection.logTag, err.Error())
				conn.Close()
				continue
			}

			bytesReadBuffer = bytesReadBuffer[0:bytesRead]

			//determine the bind mode, and pass the accepted socket
			commandId := utils.ExtractIntegerFromArray(bytesReadBuffer[4:8])
			switch commandId {
			case SmppCommandIdBindReceiver:
				l4g.Debug("<%s>Incoming BindReceiver. Initializing passiveReceiveSocket", connection.logTag)
				go connection.passiveRcvSocket.socketAccepted(conn, bytesReadBuffer)
				break
			case SmppCommandIdBindTransmitter:
				l4g.Debug("<%s>Incoming BindTransmitter. Initializing passiveTransmitSocket", connection.logTag)
				go connection.passiveTrxSocket.socketAccepted(conn, bytesReadBuffer)
				break
			case SmppCommandIdBindTranceiver:
				l4g.Debug("<%s>Incoming BindTransceiver. Initializing passiveTransmitSocket", connection.logTag)
				go connection.passiveTrxSocket.socketAccepted(conn, bytesReadBuffer)
				break
			default:
				l4g.Error("<%s>Error trying to determine type of bindmode on new connection. Closing connection", connection.logTag)
				conn.Close()
				continue
			}
		}
	}
}
