package socket

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	l4g "code.google.com/p/log4go"
	"fmt"
	"net"
	"sync"
	"time"
)

const maxStateHistory = 5
const socketNonHammerTime = 3

type StateHistoryDebug struct {
	state     int
	timeStamp time.Time
}

type SocketBase struct {
	//bytes which are received and which need to be sent. These are just raw bytes. We don't know anything about the protocol
	InRawChannel    chan []byte //bytes to sent
	InResetChannel  chan int    //put something in here to reconnect socket
	OutRawChannel   chan []byte //bytes received
	OutResetChannel chan int    //something will be put in here if the socket needed to reconnect

	//internal stuff
	socket              net.Conn  //actual socket
	socketLastTimeStamp time.Time //timestamp last time something happened
	state               int       //Use the socketState consts

	//logging / debug stuff
	logTag       string
	stateHistory []StateHistoryDebug

	//easy access to the configuration
	transportLayerConfig *config.TransportLayerConfig

	//connection handler
	connectionHandler SocketConnectorInterface
}

func (connection *SocketBase) Description() string {
	response := ""

	first := true
	for i := len(connection.stateHistory) - 1; i >= 0; i-- {
		if !first {
			response += "<font size=-2>"
		}
		stateHistoricDataPoint := connection.stateHistory[i]
		response += socketStateToString(stateHistoricDataPoint.state)

		response += fmt.Sprintf("(since %d seconds)<br>", time.Now().Sub(stateHistoricDataPoint.timeStamp)/time.Second)
		if !first {
			response += "</font>"
		}
		first = false
	}

	return response
}

func socketStateToString(state int) string {
	switch state {
	case SocketStateUnconnected:
		return "Unconnected"
	case SocketStateConnected:
		return "Connected"
	case SocketStateReconnect:
		return "Reconnect"
	case SocketStateError:
		return "Error"
	}

	return "Unknown"
}

func (connection *SocketBase) InitChannels(logTag string) {
	connection.InRawChannel = make(chan []byte)
	connection.InResetChannel = make(chan int)
	connection.logTag = logTag
	connection.state = SocketStateUnconnected
	connection.stateHistory = make([]StateHistoryDebug, 0)
}

func (connection *SocketBase) Start(config *config.TransportLayerConfig, connectionHandler SocketConnectorInterface) {
	connection.transportLayerConfig = config
	l4g.Info("<%s>Initialize socket", connection.logTag)

	connection.connectionHandler = connectionHandler

	connection.initConnection()

	go connection.connectionHandler.connectionGoRoutine()
	go connection.sendGoRoutine()
	go connection.receiveGoRoutine()
	go connection.resetGoRoutine()
}

func (connection *SocketBase) GetInboundResetChannel() chan int {
	return connection.InResetChannel
}

func (connection *SocketBase) GetInboundDataChannel() chan []byte {
	return connection.InRawChannel
}

func (connection *SocketBase) SetOutboundResetChannel(outResetChannel chan int) {
	connection.OutResetChannel = outResetChannel
}

func (connection *SocketBase) SetOutboundDataChannel(outRawChannel chan []byte) {
	connection.OutRawChannel = outRawChannel
}

func (connection *SocketBase) initConnection() {
	connection.socketLastTimeStamp = time.Now().Local()
	connection.changeState(SocketStateUnconnected)
	connection.socket = nil
}

func (connection *SocketBase) resetGoRoutine() {
	for {
		select {
		case _ = <-connection.InResetChannel:
			if connection.state == SocketStateConnected {
				l4g.Info("<%s>Socket is going to reconnect because it was requested by the protocol", connection.logTag)
				connection.changeState(SocketStateError)
			} else {
				l4g.Info("<%s>protocol requesting reconnect, but we were already unconnected", connection.logTag)
			}
		case <-time.After(5 * time.Second):
			break
		}
	}
}

func (connection *SocketBase) sendGoRoutine() {
	var bytesToSend []byte = nil //if a write didn't send all bytes in 1 go, we store it here and retry in the next loop
	var bytesDone int = 0

	for {
		l4g.Debug("<%s>socket sendgoroutine state: %s", connection.logTag, socketStateToString(connection.state))
		switch connection.state {
		case SocketStateConnected:
			//either get new buffer to send
			if bytesToSend == nil {
				select {
				case bytesToSend = <-connection.InRawChannel:
					bytesDone = 0
				case <-time.After(5 * time.Second):
					//we make sure every 5 seconds we break away from the loop, to never have a situation where a goroutine hangs indefinately
				}
			} else {
				//send the remainder of the buffer
				l4g.Debug("<%s>%d bytes ready to sent!", connection.logTag, len(bytesToSend))
				connection.socket.SetWriteDeadline(time.Now().Add(30 * time.Second))
				bytesSent, err := connection.socket.Write(bytesToSend[bytesDone : len(bytesToSend)-bytesDone])
				if err != nil {
					l4g.Error("<%s>Error while trying to send data on the socket. Err = %s", connection.logTag, err.Error())
					connection.changeState(SocketStateError)
					continue
				}
				l4g.Debug("<%s>%d actual bytes sent!", connection.logTag, bytesSent)
				//update how much we sent already
				bytesDone += bytesSent
				//check if we send everything
				if bytesDone >= len(bytesToSend) {
					bytesToSend = nil
					bytesDone = 0
				}

			}
		default:
			bytesDone = 0
			bytesToSend = nil
			time.Sleep(socketNonHammerTime * time.Second)
		}

	}
}

func (connection *SocketBase) receiveGoRoutine() {
	var bytesReadBuffer []byte
	for {
		l4g.Debug("<%s>socket receivegoroutine in state: %s", connection.logTag, socketStateToString(connection.state))
		switch connection.state {
		case SocketStateConnected:
			bytesRead := 0

			//read any data of the socket
			bytesReadBuffer = make([]byte, 1024)
			connection.socket.SetReadDeadline(time.Now().Add(30 * time.Second))
			bytesRead, err := connection.socket.Read(bytesReadBuffer)
			l4g.Debug("<%s>Bytes read %d", connection.logTag, bytesRead)
			//filter out timouts which don't mean a thing
			if err != nil {
				switch err.(type) {
				case net.Error:
					{
						if err.(net.Error).Timeout() {
							err = nil //time out reached
						}
					}
				}
			}
			//check for actual errors
			if err != nil {
				l4g.Error("<%s>Error while trying to read data on the socket. Err = %s", connection.logTag, err.Error())
				connection.changeState(SocketStateError)
			} else if bytesRead == 0 {
				time.Sleep(50 * time.Millisecond) //0 bytes read usually means, socket is closed. Then again we will also get an error if that is the case
			} else {

				//see if we are still in the correct state. If so, write the received packet in the channel
				if connection.state == SocketStateConnected {
					if bytesRead > 0 && bytesReadBuffer != nil {
						bytesReadBuffer = bytesReadBuffer[0:bytesRead]
						l4g.Debug("<%s>putting the read bytes in the outputrawchannel", connection.logTag)
						connection.OutRawChannel <- bytesReadBuffer
					}
				}
			}
			break
		default:
			time.Sleep(socketNonHammerTime * time.Second)
			bytesReadBuffer = nil
			break
		}

	}
}

func (connection *SocketBase) changeState(newState int) {
	//State needs to be synchronized since it can be changed from multiple goroutines
	switch newState {
	case SocketStateError:
		connection.OutResetChannel <- 0 //signal that we are down
		break
	case SocketStateConnected:
		connection.OutResetChannel <- 1 //signal that we are up
		break
	}

	var mutex = &sync.Mutex{}
	mutex.Lock()
	connection.state = newState
	mutex.Unlock()

	//for debugging, push the state history
	stateHistoricDataPoint := StateHistoryDebug{state: newState, timeStamp: time.Now()}
	connection.stateHistory = append(connection.stateHistory, stateHistoricDataPoint)
	if len(connection.stateHistory) > maxStateHistory {
		//remove first element
		connection.stateHistory = append(connection.stateHistory[:0], connection.stateHistory[1:]...)
	}
}

func (connection *SocketBase) emptyResetChannel() {
	for {
		select {
		case _ = <-connection.InResetChannel:
		default:
			return
		}
	}
}
