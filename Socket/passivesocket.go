package socket

import (
	l4g "code.google.com/p/log4go"
	"net"
	//	"os"
	//	"strconv"
	"time"
)

type PassiveSocket struct {
	SocketBase
}

func (connection *PassiveSocket) socketAccepted(socket net.Conn, alreadyReceivedBytes []byte) {
	if connection.state == SocketStateUnconnected {
		connection.socket = socket
		connection.changeState(SocketStateConnected)
		connection.OutRawChannel <- alreadyReceivedBytes
		l4g.Info("<%s>Socket connected", connection.logTag)
		return
	} else {
		l4g.Debug("<%s>Incoming connection refused and closed since we already have an active connection", connection.logTag)
	}
}

func (connection *PassiveSocket) connectionGoRoutine() {
	for {
		switch connection.state {
		case SocketStateError:
			l4g.Debug("<%s>Closing socket")
			err := connection.socket.Close()
			if err != nil {
				l4g.Warn("<%s>Error trying to close the socket: %s", connection.logTag, err.Error())
			}
			connection.changeState(SocketStateUnconnected)
			break
		default:
		}

		time.Sleep(500 * time.Millisecond) //sleep to not keep on using resources

	}
}
