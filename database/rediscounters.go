package database

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	l4g "code.google.com/p/log4go"
	"github.com/garyburd/redigo/redis"
	"strconv"
	"time"
)

type RedisCounters struct {
	GetIDChannel             chan (chan int)
	GetAssociatedIDChannel   chan GetAssociatedIDRequest
	StoreAssociatedIDChannel chan StoreAssociatedIDRequest

	//internal stuff
	pool *redis.Pool

	//logging
	logTag string

	//easy access to the configuration
	countersConfig *config.CountersConfig
}

type GetAssociatedIDRequest struct {
	key           string
	returnChannel chan int
}

type StoreAssociatedIDRequest struct {
	key           string
	id            int
	returnChannel chan bool
}

type RedisCountersInterface interface {
	Start(configuration *config.CountersConfig)
	InitChannels(logTag string)
	GetID(returnChannel chan int)
	GetAssociatedID(key string, returnChannel chan int)
	StoreAssociatedID(key string, id int, returnChannel chan bool)
}

func (database *RedisCounters) InitChannels(logTag string) {
	database.GetIDChannel = make(chan (chan int))
	database.GetAssociatedIDChannel = make(chan GetAssociatedIDRequest)
	database.StoreAssociatedIDChannel = make(chan StoreAssociatedIDRequest)
	database.logTag = logTag
}

func (database *RedisCounters) Start(configuration *config.CountersConfig) {
	database.countersConfig = configuration
	l4g.Info("<%s>Database counters start", database.logTag)

	database.pool = database.newPool(configuration.DatabaseConfig.RedisServer, configuration.DatabaseConfig.RedisPassword)

	go database.GetIDGoRoutine()
	go database.StoreAssociatedIDGoRoutine()
	go database.GetAssociatedIDGoRoutine()
}

//Get an atomic incrementing counter
func (database *RedisCounters) GetID(returnChannel chan int) {
	database.GetIDChannel <- returnChannel
}

//Get the ID associated with a key
func (database *RedisCounters) GetAssociatedID(key string, returnChannel chan int) {
	request := GetAssociatedIDRequest{key: key, returnChannel: returnChannel}
	database.GetAssociatedIDChannel <- request
}

//Store an ID under a key
func (database *RedisCounters) StoreAssociatedID(key string, id int, returnChannel chan bool) {
	request := StoreAssociatedIDRequest{key: key, id: id, returnChannel: returnChannel}
	database.StoreAssociatedIDChannel <- request
}

//go routine which waits for a request (containing the return channel)
//then incrments a counter in the database
//and returns the counter in the return channel

func (database *RedisCounters) GetIDGoRoutine() {
	var currentRequestReturnChannel chan int = nil
	for {
		//Is there a request to fulfil?
		if currentRequestReturnChannel != nil {
			conn := database.pool.Get()
			counter, err := redis.Int(conn.Do("INCR", database.countersConfig.DatabaseConfig.KeyName))
			conn.Close()
			if err == nil {
				//database operation succeeded, return id
				currentRequestReturnChannel <- (counter & database.countersConfig.CounterWrapAroundMaxInt)

				//clear request
				currentRequestReturnChannel = nil
			} else {
				//database operation did not succeed
				//log it
				l4g.Error("<%s>Could not increment counter in database. Will keep on retrying. Error: ", database.logTag, err.Error())

				//wait for x seconds
				time.Sleep(5 * time.Second) //sleep to not keep on hammering

				//reload config
				//todo for next version... reload config dynamically
			}
		} else {
			//read the channel for stuff to store in the database
			select {
			case currentRequestReturnChannel = <-database.GetIDChannel:
			case <-time.After(5 * time.Second):
				//we make sure every 5 seconds we break away from the loop, to never have a situation where a goroutine hangs indefinately
			}

		}
	}

}

//go routine which waits for a request (containing the return channel, a key and an id)
//then stores the id under the key in the database
//and returns 'true' in the return channel

//TODO: Storing of these keys need a TTS, otherwise the database would fill up!

func (database *RedisCounters) StoreAssociatedIDGoRoutine() {
	var currentRequest StoreAssociatedIDRequest
	currentRequest.returnChannel = nil
	for {
		//Is there a request to fulfil?
		if currentRequest.returnChannel != nil {
			conn := database.pool.Get()
			fullKey := database.countersConfig.DatabaseConfig.KeyName + currentRequest.key
			_, err := conn.Do("SET", fullKey, ""+strconv.FormatInt(int64(currentRequest.id), 10))
			if err == nil {
				_, err = conn.Do("EXPIRE", fullKey, "86400") //1 day retention (TODO: get value from config)
			}
			conn.Close()
			if err == nil {
				//database operation succeeded, return bool to signal succes
				currentRequest.returnChannel <- true

				//clear request
				currentRequest.returnChannel = nil
			} else {
				//database operation did not succeed
				//log it
				l4g.Error("<%s>Could not store the alias for an external messageid in the database. Will keep on retrying. Error: %s", database.logTag, err.Error())

				//wait for x seconds
				time.Sleep(5 * time.Second) //sleep to not keep on hammering

				//reload config
				//todo for next version... reload config dynamically
			}
		} else {
			//read the channel for stuff to store in the database
			select {
			case currentRequest = <-database.StoreAssociatedIDChannel:
			case <-time.After(5 * time.Second):
				//we make sure every 5 seconds we break away from the loop, to never have a situation where a goroutine hangs indefinately
			}

		}
	}

}

//go routine which waits for a request (containing the return channel and a key)
//then gets the associated id under the key in the database
//and returns the id in the return channel

func (database *RedisCounters) GetAssociatedIDGoRoutine() {
	var currentRequest GetAssociatedIDRequest
	currentRequest.returnChannel = nil
	for {
		//Is there a request to fulfil?
		if currentRequest.returnChannel != nil {
			conn := database.pool.Get()
			fullKey := database.countersConfig.DatabaseConfig.KeyName + currentRequest.key
			id, err := redis.Int(conn.Do("GET", fullKey))
			conn.Close()
			if err == nil {
				//database operation succeeded, return the associated id to signal succes
				currentRequest.returnChannel <- id

				//clear request
				currentRequest.returnChannel = nil
			} else {
				// check if the database doesn't even contain this entry
				if err == redis.ErrNil {
					l4g.Error("<%s>The alias for an external messageid in the database was not stored.. Will return a wrong default value (0). This should always work and will result in faulty delivnotification counting", database.logTag)
					//just send back a default value.
					currentRequest.returnChannel <- 0
					currentRequest.returnChannel = nil
				} else {
					//database operation did not succeed
					//log it
					l4g.Error("<%s>Could not retrieve the alias for an external messageid in the database. Will keep on retrying. This has to be fixed before this communication resumes!. Error: %s", database.logTag, err.Error())

					//wait for x seconds
					time.Sleep(5 * time.Second) //sleep to not keep on hammering

					//reload config
					//todo for next version... reload config dynamically
				}

			}
		} else {
			//read the channel for stuff to store in the database
			select {
			case currentRequest = <-database.GetAssociatedIDChannel:
			case <-time.After(5 * time.Second):
				//we make sure every 5 seconds we break away from the loop, to never have a situation where a goroutine hangs indefinately
			}

		}
	}

}

func (database *RedisCounters) newPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     int(database.countersConfig.DatabaseConfig.MaxIdleConnections),
		IdleTimeout: time.Duration(database.countersConfig.DatabaseConfig.MaxTimeConnectionIdleInSeconds) * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				l4g.Error("<%s>Can not connect to the database: %s", database.logTag, err.Error())
				return nil, err
			}
			if len(password) != 0 {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					l4g.Error("<%s>Authorization for the database not succeeded: %s", database.logTag, err.Error())
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}
