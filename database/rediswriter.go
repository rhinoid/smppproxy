package database

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
)

type RedisWriter struct {
	InPDUChannel chan utils.PDUBytes

	//internal stuff
	pool *redis.Pool

	//logging / debugging
	logTag              string
	timeSinceLastPacket time.Time
	numPdusInDatabase   int

	//easy access to the configuration
	databaseConfig *config.DatabaseConfig
}

func (database *RedisWriter) QueueAsDescription() string {
	return fmt.Sprintf("%d", database.numPdusInDatabase)
}

func (database *RedisWriter) Description() string {
	lastPacketSince := fmt.Sprintf("%d packets in database (last packet %d seconds ago)", database.numPdusInDatabase, time.Now().Sub(database.timeSinceLastPacket)/time.Second)

	return lastPacketSince
}

func (database *RedisWriter) InitChannels(logTag string) {
	database.InPDUChannel = make(chan utils.PDUBytes)
	database.logTag = logTag
	database.timeSinceLastPacket = time.Now()
	database.numPdusInDatabase = 0
}

func (database *RedisWriter) Start(configuration *config.DatabaseConfig) {
	database.databaseConfig = configuration
	l4g.Info("<%s>Start rediswriter", database.logTag)

	database.pool = database.newPool(configuration.RedisServer, configuration.RedisPassword)

	go database.writeDatabaseWithDataGoRoutine()
	go database.databaseStatsGoRoutine()
}

func (database *RedisWriter) GetInboundDataChannel() chan utils.PDUBytes {
	return database.InPDUChannel
}

func (database *RedisWriter) databaseStatsGoRoutine() {
	for {
		conn := database.pool.Get()

		//read how many pdu's there are in the queue
		numPdus, err := redis.Int(conn.Do("LLEN", database.databaseConfig.KeyName))
		conn.Close()
		if err == nil {
			database.numPdusInDatabase = numPdus
		}
		time.Sleep(5 * time.Second) //check stats every 5 seconds
	}
}

func (database *RedisWriter) writeDatabaseWithDataGoRoutine() {
	var bytesToStore utils.PDUBytes = nil
	for {

		// Is there still data to store?
		if bytesToStore != nil {
			conn := database.pool.Get()
			_, err := conn.Do("RPUSH", database.databaseConfig.KeyName, []byte(bytesToStore))
			conn.Close()
			if err == nil {
				//database store succeeded
				bytesToStore = nil
				database.timeSinceLastPacket = time.Now()
			} else {
				//database store did not succeed
				//log it
				l4g.Error("<%s>Could not write to database. Will keep on retrying. Error: ", database.logTag, err.Error())

				//wait for x seconds
				time.Sleep(5 * time.Second) //sleep to not keep on hammering

				//reload config
				//todo for next version... reload config dynamically
			}
		} else {
			//read the channel for stuff to store in the database
			select {
			case bytesToStore = <-database.InPDUChannel:
			case <-time.After(5 * time.Second):
				//we make sure every 5 seconds we break away from the loop, to never have a situation where a goroutine hangs indefinately
			}

		}
	}

}

func (database *RedisWriter) newPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     int(database.databaseConfig.MaxIdleConnections),
		IdleTimeout: time.Duration(database.databaseConfig.MaxTimeConnectionIdleInSeconds) * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				l4g.Error("<%s>Can not connect to the database: %s", database.logTag, err.Error())
				return nil, err
			}
			if len(password) != 0 {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					l4g.Error("<%s>Authorization for the database not succeeded: %s", database.logTag, err.Error())
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}
