package database

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
	l4g "code.google.com/p/log4go"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"time"
)

type RedisReader struct {
	OutPDUChannel chan utils.PDUBytes

	//internal stuff
	pool *redis.Pool

	//logging / debugging
	logTag              string
	timeSinceLastPacket time.Time
	numPdusInDatabase   int

	//easy access to the configuration
	databaseConfig *config.DatabaseConfig
}

func (database *RedisReader) Description() string {
	lastPacketSince := fmt.Sprintf("%d packets in database (last packet %d seconds ago)", database.numPdusInDatabase, time.Now().Sub(database.timeSinceLastPacket)/time.Second)

	return lastPacketSince
}

func (database *RedisReader) InitChannels(logTag string) {
	database.logTag = logTag
	database.timeSinceLastPacket = time.Now()
	database.numPdusInDatabase = 0
}

func (database *RedisReader) SetOutboundDataChannel(outPDUChannel chan utils.PDUBytes) {
	database.OutPDUChannel = outPDUChannel
}

func (database *RedisReader) Start(configuration *config.DatabaseConfig) {
	database.databaseConfig = configuration
	l4g.Info("<%s>Start redisreader", database.logTag)

	database.pool = database.newPool(configuration.RedisServer, configuration.RedisPassword)

	go database.readDatabaseForDataGoRoutine()
	go database.databaseStatsGoRoutine()
}

func (database *RedisReader) databaseStatsGoRoutine() {
	for {
		conn := database.pool.Get()

		//read how many pdu's there are in the queue
		numPdus, err := redis.Int(conn.Do("LLEN", database.databaseConfig.KeyName))
		conn.Close()
		if err == nil {
			database.numPdusInDatabase = numPdus
		}
		time.Sleep(5 * time.Second) //check stats every 5 seconds
	}
}

func (database *RedisReader) readDatabaseForDataGoRoutine() {
	for {
		conn := database.pool.Get()

		//get the next PDU from redis and block if needed
		pduStrings, err := redis.Strings(conn.Do("BLPOP", database.databaseConfig.KeyName, database.databaseConfig.BlockingReadTimeOutInSeconds))
		conn.Close()
		if err != nil {
			//some kind of database error occurred.
			//There is nothing more we can do then log the error, sleep for 5 seconds, and keep on retrying
			l4g.Error("<%s>BLPOP from the redis database failed. We will retry in 5 seconds. The error was: %s", database.logTag, err.Error())
			time.Sleep(5 * time.Second) //sleep to not keep on using resources if something goes wrong.
		} else {
			pduBytes := []byte(pduStrings[1])
			if pduBytes != nil && len(pduBytes) > 0 {
				l4g.Debug("<%s>retrieved PDU from DB with len: %d", database.logTag, len(pduBytes))
				database.OutPDUChannel <- pduBytes
				database.timeSinceLastPacket = time.Now()
			} else {
				l4g.Error("<%s>could not convert to bytes?", database.logTag)
			}
		}

	}
}

func (database *RedisReader) newPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     int(database.databaseConfig.MaxIdleConnections),
		IdleTimeout: time.Duration(database.databaseConfig.MaxTimeConnectionIdleInSeconds) * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				l4g.Error("<%s>Can not connect to the database: %s", database.logTag, err.Error())
				return nil, err
			}
			if len(password) != 0 {
				if _, err := c.Do("AUTH", password); err != nil {
					c.Close()
					l4g.Error("<%s>Authorization for the database not succeeded: %s", database.logTag, err.Error())
					return nil, err
				}
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}
