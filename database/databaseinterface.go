package database

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/utils"
)

type DatabaseReaderInterface interface {
	InitChannels(logTag string)
	Start(configuration *config.DatabaseConfig, outPDUChannel chan utils.PDUBytes)
}

type DatabaseWriterInterface interface {
	InitChannels(logTag string)
	Start(configuration *config.DatabaseConfig)
}
