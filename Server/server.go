/*
The server package will be the central hub in owning both the active and passive part of the proxy with each it's transmitters and recievers.
Other than owning all the parts under it (you can see it as the root object) it doesn't serve much of a function (yet)
*/

package server

import (
	"bitbucket.org/rhinoid/SMPPProxy/config"
	"bitbucket.org/rhinoid/SMPPProxy/database"
	"bitbucket.org/rhinoid/SMPPProxy/protocol"
	"bitbucket.org/rhinoid/SMPPProxy/socket"
	//	l4g "code.google.com/p/log4go"
	"fmt"
	"net/http"
)

type ActiveServerPart struct {
	activeTransmitSocket   socket.ActiveSocket
	activeReceiveSocket    socket.ActiveSocket
	activeTransmitProtocol protocol.Transmitter
	activeReceiveProtocol  protocol.Receiver
	activeDatabaseReader   database.RedisReader
	activeDatabaseWriter   database.RedisWriter

	activeTransceiverProtocol protocol.Transceiver
}

type ServerDetails struct {

	//easy access to the configuration
	configuration *config.Configuration

	//counter rewriting (for the receipts)
	redisCounters database.RedisCounters

	//active part of the connection (the part which actively tries to seek a connection to a remote server)
	activeServerPart []*ActiveServerPart

	//passive part of the connection (the part which waits for incoming connections from the mads server)
	acceptSocket            socket.AcceptSocket
	passiveTransmitSocket   socket.PassiveSocket
	passiveReceiveSocket    socket.PassiveSocket
	passiveTransmitProtocol protocol.PassiveTransmitter
	passiveReceiveProtocol  protocol.PassiveReceiver
	passiveDatabaseReader   database.RedisReader
	passiveDatabaseWriter   database.RedisWriter
}

func (server ServerDetails) Start(configuration *config.Configuration) {
	server.configuration = configuration

	fmt.Println("<Server>Initializing server")

	/////////////
	//Start database counters (for rewriting the sms receipts ID's)
	server.redisCounters = database.RedisCounters{}
	server.redisCounters.InitChannels("DatabaseCounters")
	server.redisCounters.Start(&configuration.CountersConfig)

	///////////////////////////////////////////
	//Hook up the active part of the connection
	server.activeServerPart = make([]*ActiveServerPart, configuration.ActiveConnection.NumberOfConnections)
	//shoudl the active part connect as a transceiver or as a separate transmitter/receiver?
	for i := 0; i < int(configuration.ActiveConnection.NumberOfConnections); i++ {
		newActiveServer := ActiveServerPart{}
		if configuration.ActiveConnection.TransceiverMode {
			//Set up transceiver connection
			//transceiver

			//initialize structs
			newActiveServer.activeTransmitSocket = socket.ActiveSocket{}
			newActiveServer.activeTransceiverProtocol = protocol.Transceiver{}
			newActiveServer.activeDatabaseReader = database.RedisReader{}
			newActiveServer.activeDatabaseWriter = database.RedisWriter{}

			//init inbound channels
			logTag := fmt.Sprintf("ActiveTransceiver %d", i)
			newActiveServer.activeTransmitSocket.InitChannels(logTag)
			newActiveServer.activeTransceiverProtocol.InitChannels(logTag)
			newActiveServer.activeDatabaseReader.InitChannels(logTag)
			newActiveServer.activeDatabaseWriter.InitChannels(logTag)

			//hookup channels
			newActiveServer.activeTransmitSocket.SetOutboundDataChannel(newActiveServer.activeTransceiverProtocol.GetInboundTransmitChannel())
			newActiveServer.activeTransmitSocket.SetOutboundResetChannel(newActiveServer.activeTransceiverProtocol.GetInboundResetTransportChannel())

			newActiveServer.activeTransceiverProtocol.SetOutboundTransportChannel(newActiveServer.activeTransmitSocket.GetInboundDataChannel())
			newActiveServer.activeTransceiverProtocol.SetOutboundTransportResetChannel(newActiveServer.activeTransmitSocket.GetInboundResetChannel())
			newActiveServer.activeTransceiverProtocol.SetOutboundStorageChannel(newActiveServer.activeDatabaseWriter.GetInboundDataChannel())

			newActiveServer.activeDatabaseReader.SetOutboundDataChannel(newActiveServer.activeTransceiverProtocol.GetInboundStorageChannel())

			newActiveServer.activeTransceiverProtocol.SetDatabaseCounters(&server.redisCounters)

			//start active transmitter flow
			fmt.Println("<Server>Starting active transceiver flow")
			newActiveServer.activeDatabaseReader.Start(&configuration.ActiveConnection.TransmitterConfig.DatabaseConfig)
			newActiveServer.activeDatabaseWriter.Start(&configuration.ActiveConnection.ReceiverConfig.DatabaseConfig)

			newActiveServer.activeTransceiverProtocol.Start(&configuration.ActiveConnection.TransmitterConfig.SmppProtocolConfig, &newActiveServer.activeTransceiverProtocol)

			newActiveServer.activeTransmitSocket.Start(&configuration.ActiveConnection.TransportLayerConfig, &newActiveServer.activeTransmitSocket)
		} else {
			//Set up transmitter and reciever
			//transmitter

			//initialize structs
			newActiveServer.activeTransmitSocket = socket.ActiveSocket{}
			newActiveServer.activeTransmitProtocol = protocol.Transmitter{}
			newActiveServer.activeDatabaseReader = database.RedisReader{}

			//init inbound channels
			logTag := fmt.Sprintf("ActiveTransmit %d", i)
			newActiveServer.activeTransmitSocket.InitChannels(logTag)
			newActiveServer.activeTransmitProtocol.InitChannels(logTag)
			newActiveServer.activeDatabaseReader.InitChannels(logTag)

			//hookup channels
			newActiveServer.activeTransmitSocket.SetOutboundDataChannel(newActiveServer.activeTransmitProtocol.GetInboundTransmitChannel())
			newActiveServer.activeTransmitSocket.SetOutboundResetChannel(newActiveServer.activeTransmitProtocol.GetInboundResetTransportChannel())

			newActiveServer.activeTransmitProtocol.SetOutboundTransportChannel(newActiveServer.activeTransmitSocket.GetInboundDataChannel())
			newActiveServer.activeTransmitProtocol.SetOutboundTransportResetChannel(newActiveServer.activeTransmitSocket.GetInboundResetChannel())
			newActiveServer.activeTransmitProtocol.SetOutboundStorageChannel(nil)

			newActiveServer.activeDatabaseReader.SetOutboundDataChannel(newActiveServer.activeTransmitProtocol.GetInboundStorageChannel())

			newActiveServer.activeTransmitProtocol.SetDatabaseCounters(&server.redisCounters)

			//start active transmitter flow
			fmt.Println("<Server>Starting active transmitter flow")
			newActiveServer.activeDatabaseReader.Start(&configuration.ActiveConnection.TransmitterConfig.DatabaseConfig)

			newActiveServer.activeTransmitProtocol.Start(&configuration.ActiveConnection.TransmitterConfig.SmppProtocolConfig, &newActiveServer.activeTransmitProtocol)

			newActiveServer.activeTransmitSocket.Start(&configuration.ActiveConnection.TransportLayerConfig, &newActiveServer.activeTransmitSocket)

			//reciever

			//initialize structs
			newActiveServer.activeReceiveSocket = socket.ActiveSocket{}
			newActiveServer.activeReceiveProtocol = protocol.Receiver{}
			newActiveServer.activeDatabaseWriter = database.RedisWriter{}

			//init inbound channels
			logTag = fmt.Sprintf("ActiveReceive %d", i)
			newActiveServer.activeReceiveSocket.InitChannels(logTag)
			newActiveServer.activeReceiveProtocol.InitChannels(logTag)
			newActiveServer.activeDatabaseWriter.InitChannels(logTag)

			//hookup channels
			newActiveServer.activeReceiveSocket.SetOutboundDataChannel(newActiveServer.activeReceiveProtocol.GetInboundTransmitChannel())
			newActiveServer.activeReceiveSocket.SetOutboundResetChannel(newActiveServer.activeReceiveProtocol.GetInboundResetTransportChannel())

			newActiveServer.activeReceiveProtocol.SetOutboundTransportChannel(newActiveServer.activeReceiveSocket.GetInboundDataChannel())
			newActiveServer.activeReceiveProtocol.SetOutboundTransportResetChannel(newActiveServer.activeReceiveSocket.GetInboundResetChannel())
			newActiveServer.activeReceiveProtocol.SetOutboundStorageChannel(newActiveServer.activeDatabaseWriter.GetInboundDataChannel())

			newActiveServer.activeReceiveProtocol.SetDatabaseCounters(&server.redisCounters)

			//start active receiver flow
			fmt.Println("<Server>Starting active receiver flow")
			newActiveServer.activeDatabaseWriter.Start(&configuration.ActiveConnection.ReceiverConfig.DatabaseConfig)

			newActiveServer.activeReceiveProtocol.Start(&configuration.ActiveConnection.ReceiverConfig.SmppProtocolConfig, &newActiveServer.activeReceiveProtocol)

			newActiveServer.activeReceiveSocket.Start(&configuration.ActiveConnection.TransportLayerConfig, &newActiveServer.activeReceiveSocket)
		}
		server.activeServerPart[i] = &newActiveServer
	}

	///////////////////////////////////////////
	//Hook up the passive part of the connection

	server.acceptSocket = socket.AcceptSocket{}
	server.acceptSocket.InitChannels("ListenSocket")

	fmt.Println("<Server>Initializing passive flows")
	//initialize structs
	server.passiveTransmitSocket = socket.PassiveSocket{}
	server.passiveTransmitProtocol = protocol.PassiveTransmitter{}
	server.passiveDatabaseWriter = database.RedisWriter{}

	//init inbound channels
	server.passiveTransmitSocket.InitChannels("PassiveTransmit")
	server.passiveTransmitProtocol.InitChannels("PassiveTransmit")
	server.passiveDatabaseWriter.InitChannels("PassiveTransmit")

	//hookup channels
	server.passiveTransmitSocket.SetOutboundDataChannel(server.passiveTransmitProtocol.GetInboundTransmitChannel())
	server.passiveTransmitSocket.SetOutboundResetChannel(server.passiveTransmitProtocol.GetInboundResetTransportChannel())

	server.passiveTransmitProtocol.SetOutboundTransportChannel(server.passiveTransmitSocket.GetInboundDataChannel())
	server.passiveTransmitProtocol.SetOutboundTransportResetChannel(server.passiveTransmitSocket.GetInboundResetChannel())
	server.passiveTransmitProtocol.SetOutboundStorageChannel(server.passiveDatabaseWriter.GetInboundDataChannel())

	server.passiveTransmitProtocol.SetDatabaseCounters(&server.redisCounters)

	//start passive transmitter flow
	fmt.Println("<Server>Starting passive transmitter flow")
	server.passiveDatabaseWriter.Start(&configuration.PassiveConnection.TransmitterConfig.DatabaseConfig)

	server.passiveTransmitProtocol.Start(&configuration.PassiveConnection.TransmitterConfig.SmppProtocolConfig, &server.passiveTransmitProtocol)

	server.passiveTransmitSocket.Start(&configuration.PassiveConnection.TransportLayerConfig, &server.passiveTransmitSocket)

	//initialize structs
	server.passiveReceiveSocket = socket.PassiveSocket{}
	server.passiveReceiveProtocol = protocol.PassiveReceiver{}
	server.passiveDatabaseReader = database.RedisReader{}

	//init inbound channels
	server.passiveReceiveSocket.InitChannels("PassiveReceive")
	server.passiveReceiveProtocol.InitChannels("PassiveReceive")
	server.passiveDatabaseReader.InitChannels("PassiveReceive")

	//hookup channels
	server.passiveReceiveSocket.SetOutboundDataChannel(server.passiveReceiveProtocol.GetInboundTransmitChannel())
	server.passiveReceiveSocket.SetOutboundResetChannel(server.passiveReceiveProtocol.GetInboundResetTransportChannel())

	server.passiveReceiveProtocol.SetOutboundTransportChannel(server.passiveReceiveSocket.GetInboundDataChannel())
	server.passiveReceiveProtocol.SetOutboundTransportResetChannel(server.passiveReceiveSocket.GetInboundResetChannel())
	server.passiveReceiveProtocol.SetOutboundStorageChannel(nil)

	server.passiveDatabaseReader.SetOutboundDataChannel(server.passiveReceiveProtocol.GetInboundStorageChannel())

	//start passive transmitter flow
	fmt.Println("<Server>Starting passive receiver flow")
	server.passiveDatabaseReader.Start(&configuration.PassiveConnection.ReceiverConfig.DatabaseConfig)

	server.passiveReceiveProtocol.Start(&configuration.PassiveConnection.ReceiverConfig.SmppProtocolConfig, &server.passiveReceiveProtocol)
	server.passiveReceiveSocket.Start(&configuration.PassiveConnection.TransportLayerConfig, &server.passiveReceiveSocket)

	//the accept socket which connects the 2 passive sockets
	server.acceptSocket.Start(&configuration.PassiveConnection.TransportLayerConfig, &server.passiveReceiveSocket, &server.passiveTransmitSocket)

	//start a webserver giving insight into what the proxy is doing
	server.startWebServer()

}

func (server *ServerDetails) startWebServer() {
	go server.webServerGoRoutine()
}

func (server *ServerDetails) webServerGoRoutine() {

	handler := func(w http.ResponseWriter, r *http.Request) {
		response := ""
		if r.URL.Path == "/zabbix" {
			response += "state_mads_tx " + server.passiveTransmitProtocol.IsBoundDescription() + "\n"
			response += "state_mads_rx " + server.passiveReceiveProtocol.IsBoundDescription() + "\n"
			if server.configuration.ActiveConnection.TransceiverMode {
				response += "state_smsc_tx " + server.activeServerPart[0].activeTransceiverProtocol.IsBoundDescription() + "\n"
				response += "state_smsc_rx " + server.activeServerPart[0].activeTransceiverProtocol.IsBoundDescription() + "\n"
			} else {
				response += "state_smsc_tx " + server.activeServerPart[0].activeTransmitProtocol.IsBoundDescription() + "\n"
				response += "state_smsc_rx " + server.activeServerPart[0].activeReceiveProtocol.IsBoundDescription() + "\n"
			}
			response += "speed_mads_tx " + server.passiveTransmitProtocol.InBoundSpeedDescription() + "\n"
			response += "speed_mads_rx " + server.passiveReceiveProtocol.OutBoundSpeedDescription() + "\n"
			if server.configuration.ActiveConnection.TransceiverMode {
				response += "speed_smsc_tx " + server.activeServerPart[0].activeTransceiverProtocol.OutBoundSpeedDescription() + "\n"
				response += "speed_smsc_rx " + server.activeServerPart[0].activeTransceiverProtocol.InBoundSpeedDescription() + "\n"
			} else {
				response += "speed_smsc_tx " + server.activeServerPart[0].activeTransmitProtocol.OutBoundSpeedDescription() + "\n"
				response += "speed_smsc_rx " + server.activeServerPart[0].activeReceiveProtocol.InBoundSpeedDescription() + "\n"
			}
			response += "queue_tx " + server.passiveDatabaseWriter.QueueAsDescription() + "\n"
			response += "queue_rx " + server.activeServerPart[0].activeDatabaseWriter.QueueAsDescription() + "\n"
		} else {

			//build up a response of the current state of affairs
			//passive transmitter
			response += "<b>Flow1a: Passive Transmitter (Mads => Proxy):</b><br>"
			response += "<b>Socket:</b><br>" + server.passiveTransmitSocket.Description()
			response += "<b>Protocol:</b><br>" + server.passiveTransmitProtocol.Description()
			response += "<b>Database:</b>\r\n" + server.passiveDatabaseWriter.Description() + "<br>"

			//active transmitter
			response += "<br><b>Flow1b:Active Transmitter Flow (Proxy => SMSC):</b><br>"
			for i := 0; i < int(server.configuration.ActiveConnection.NumberOfConnections); i++ {
				response += fmt.Sprintf("<i>connection %d</i><br>", i)
				activeServerPart := server.activeServerPart[i]
				response += "<b>Database:</b>\r\n" + activeServerPart.activeDatabaseReader.Description() + "<br>"
				if server.configuration.ActiveConnection.TransceiverMode {
					response += "<b>Protocol:</b><br>" + activeServerPart.activeTransceiverProtocol.Description()
				} else {
					response += "<b>Protocol:</b><br>" + activeServerPart.activeTransmitProtocol.Description()
				}
				response += "<b>Socket:</b><br>" + activeServerPart.activeTransmitSocket.Description()
			}

			response += "<br>"
			//active receiver
			response += "<br><b>Flow2a:Active Receiver Flow (SMSC => Proxy):</b><br>"
			for i := 0; i < int(server.configuration.ActiveConnection.NumberOfConnections); i++ {
				response += fmt.Sprintf("<i>connection %d</i><br>", i)
				activeServerPart := server.activeServerPart[i]
				response += "<b>Socket:</b><br>" + activeServerPart.activeReceiveSocket.Description()
				if server.configuration.ActiveConnection.TransceiverMode {
					response += "<b>Protocol:</b><br>" + activeServerPart.activeTransceiverProtocol.Description()
				} else {
					response += "<b>Protocol:</b><br>" + activeServerPart.activeReceiveProtocol.Description()
				}
				response += "<b>Database:</b>\r\n" + activeServerPart.activeDatabaseWriter.Description() + "<br>"
			}
			//passive reciever
			response += "<br><b>Flow2b:Active Receiver Flow (Proxy => Mads):</b><br>"
			response += "<b>Database:</b>\r\n" + server.passiveDatabaseReader.Description() + "<br>"
			response += "<b>Protocol:</b><br>" + server.passiveReceiveProtocol.Description()
			response += "<b>Socket:</b><br>" + server.passiveReceiveSocket.Description()

		}
		fmt.Fprintf(w, response)
	}

	ipAndPort := fmt.Sprintf(":%d", server.configuration.WebServerConfig.Port)
	http.HandleFunc("/", handler)
	http.ListenAndServe(ipAndPort, nil)
}
