This is an SMPP store and forward proxy.

It can be used in between an ESME and an SMSC.
So in a basic setup, the ESME would connect to the proxy (running locally in the network) and send its messages.
The proxy will then try and contact the SMSC and send the messages onwards. Any delivery notifications or inbound SMSs will be delivered by the SMSC to the proxy. The proxy will then store these messages until a client will read them or process them again.

The proxy can send and receive independently in transmitter/receiver or transceiver mode. Enabling ESMEs which only support one of these protocol variations to communicate with an SMSC which supports the other.

This was one of my first projects in Go. Learned a lot. Great simple language.
